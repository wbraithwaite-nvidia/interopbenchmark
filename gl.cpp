/*
 * Copyright 1993-2012 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

#define NOMINMAX
#include "gl_utils.h"
#include "std_utils.h"
#include <cstdio>
#include <algorithm>
#include <cmath>
#include <GL/freeglut.h>

//---------------------------------------------------------------------------------------------------
GLuint vshader;
GLuint fshader;
GLuint program;
GLint programTexLoc;
GLint programItersLoc;
GLint programValueLoc;
GLint pointRadiusLoc;

//---------------------------------------------------------------------------------------------------
GLsync g_glSyncId = 0;
GLuint g_displayTexId = 0;

//---------------------------------------------------------------------------------------------------
extern int g_windowWidth;
extern int g_windowHeight;
extern int g_numIterations;
extern GLuint g_displayTexId;

//---------------------------------------------------------------------------------------------------
void initGL()
{
    const char *vtext = "\
        void main() \n\
        { \n\
            gl_FrontColor = gl_Color; \n\
            gl_Position = ftransform(); \n\
        }";
    const char *ftext = "\
        uniform sampler2D tex; \n\
        uniform int iters; \n\
        uniform float value; \n\
        void main() \n\
        { \n\
            float x = value; \n\
            for (int i = 0; i < iters; ++i) \n\
             { \n\
                x = sqrt(x) + 0.001; \n\
            } \n\
            gl_FragColor = x*gl_Color*texture2D(tex,gl_TexCoord[0].st); \n\
        }";
    vshader = glCreateShader(GL_VERTEX_SHADER);
    fshader = glCreateShader(GL_FRAGMENT_SHADER); 
    glShaderSource(vshader, 1, &vtext, NULL);
    glShaderSource(fshader, 1, &ftext, NULL);
    glCompileShader(vshader);
    glCompileShader(fshader);
    program = glCreateProgram();
    glAttachShader(program, vshader);
    glAttachShader(program, fshader);
    glLinkProgram(program);
    programTexLoc   = glGetUniformLocation(program, "tex");
    programItersLoc = glGetUniformLocation(program, "iters");
    programValueLoc = glGetUniformLocation(program, "value");
    GL_CHECK_ERRORS();
}

//---------------------------------------------------------------------------------------------------
void cleanupGL()
{
    glDeleteProgram(program);
}

//---------------------------------------------------------------------------------------------------
void drawLogo(const int frame)
{
	/*glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-1,1,-1,1,-1,1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();*/

    int size = g_windowWidth;
    if (g_windowHeight < size)
        size = g_windowHeight;
    size *= 0.9;

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    //glTranslatef(size/2, size/2, 0);
    glTranslatef(g_windowWidth/2, g_windowHeight/2, 0);
	//glRotatef((frame*360)/(g_numIterations-1), 0, 0, 1);
    static Easy::Timer timer;
    static float lastTime = 0;
    static float rotation = 0;
    float elapsedTime = std::max(0.00001f, timer.value() - lastTime);

    float x = (float)frame/g_numIterations;

    float speedFactor = cosf(x*2)*powf(1-x, 2);

    glPushMatrix();

    rotation += (1.f/elapsedTime) * speedFactor;
    glRotatef(rotation, 0, 0, 1);
    lastTime += elapsedTime;

    glTranslatef(-size/2, -size/2, 0);

    glScalef(size, size, 1);

    glBindTexture(GL_TEXTURE_2D, g_displayTexId);
        
    glEnable(GL_TEXTURE_2D);
    glColor4f(1, 1, 1, 1); 

    glBegin(GL_TRIANGLE_FAN);
    glTexCoord2f(0, 0); 
    glVertex3f(0, 0, 0);
    glTexCoord2f(0, 1); 
    glVertex3f(0, 1, 0);
    glTexCoord2f(1, 1); 
    glVertex3f(1, 1, 0);
    glTexCoord2f(1, 0); 
    glVertex3f(1, 0, 0);
    glEnd();

    glDisable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);

    glPopMatrix();

    // draw the peg...

    glColor4f(1, 1, 1, 1); 

    float pegSize = (float)size/16;
    float pegPosY = -(size*1.1)/2;

    glTranslatef(0, pegPosY, 0);
    /*
    x = fmod(rotation, 24) / 24.f;
    speedFactor = 2*sinf(x*16) * powf(1-x, 9);
    //STDERR3(rotation, x, speedFactor);
    glRotatef(speedFactor*30, 0, 0, 1);*/

    glBegin(GL_TRIANGLES);
    glVertex3f(0, pegSize-pegSize/2, 0);
    glVertex3f(pegSize/3, 0, 0);
    glVertex3f(-pegSize/3, 0, 0);

    glVertex3f(-pegSize/3, 0, 0);
    glVertex3f(pegSize/3, 0, 0);
    glVertex3f(0, pegSize, 0);
    glEnd();

    glPopMatrix();
}

//---------------------------------------------------------------------------------------------------
/// Draw using the interop GL texture.
///
void doWorkInGL(int frame, int nDelayIterations, GLuint id)
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    //glViewport(0, 0, g_windowWidth, g_windowHeight);
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);// | GL_DEPTH_BUFFER_BIT);

    // Make a really expensive draw call which uses the interop texture
    // - we make the call expensive by doing lots of iterations in a
    //   loop in the pixel shader
    // - we use a really small viewport so that the number of processors
    //   in the GPU doesn't impact performance (rather, only clocks matter,
    //   and clocks don't change as rapidly across generations)
    {
        glPushAttrib(GL_VIEWPORT_BIT);
        glViewport(0, 0, 4, 4);

        glUseProgram(program);
        glUniform1i(programTexLoc,   0);
        glUniform1i(programItersLoc, nDelayIterations);
        glUniform1f(programValueLoc, 1.001f);

        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glScalef(float(g_windowWidth), float(g_windowHeight), 1.f);

        glBegin(GL_QUADS);
        glColor4f(1, 1, 1, 1); 
        glTexCoord2f(1.0f, 1.0f); glVertex3f(4, 4, 0);
        glTexCoord2f(0.0f, 1.0f); glVertex3f(4, 0, 0);
        glTexCoord2f(0.0f, 0.0f); glVertex3f(0, 0, 0);
        glTexCoord2f(1.0f, 0.0f); glVertex3f(0, 4, 0);
        glEnd();

        glPopMatrix();
        glUseProgram(0);
        glPopAttrib();
    }

    // Draw something!
    /// CAVEAT:
    /// this slows down the iterations and makes the timing less unpredictable,
    /// so make this high performance code...
    drawLogo(frame);

    g_glSyncId = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
}

//---------------------------------------------------------------------------------------------------
