#include "utils.h"
#include <cassert>

//---------------------------------------------------------------------------------------------------
__global__ void K(unsigned char *buffer, unsigned char value, int iters)
{
    unsigned char *pixel = buffer + threadIdx.x;

    pixel[0] = value;

    for (int i = 0; i < iters; ++i) {
        pixel[0] = pixel[0] * pixel[0] + i*i;
    }
    pixel[0] &= 0xF0;
    pixel[0] |= value;
}

//---------------------------------------------------------------------------------------------------
/// Execute a long-running kernel in CUDA.
void doWorkInCUDA(int iter, int numIterations, void* buffer, size_t dataSize, cudaStream_t stream)
{
    static unsigned char value = 0;
    value += 1;
    K<<<1, 1, 0, stream>>>((unsigned char*)buffer, value, numIterations);
    CUDA_CHECK_ERRORS();
}

//---------------------------------------------------------------------------------------------------
