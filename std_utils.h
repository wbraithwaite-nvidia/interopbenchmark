/* ---------------------------------------------------------------------------
 * This software is in the public domain, furnished "as is", without technical
 * support, and with no warranty, express or implied, as to its usefulness for
 * any purpose.

 * Author: Wil Braithwaite.
 *
 */

#ifndef STD_UTILS_H_INCLUDED
#define STD_UTILS_H_INCLUDED

#include <string>
#include <iostream>
#include <cstring>
#include <map>
#include <vector>
#include <cstdarg>
#include <cstdlib>
#include <cstdio>
#include <cassert>

#if defined(_WIN32)

#ifndef NOMINMAX
#define NOMINMAX
#endif
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0502
#endif
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <stdint.h>
#include <conio.h> // for _getch
#define strcasecmp _stricmp

#else

#include <pthread.h>
#include <errno.h>
#include <semaphore.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/time.h>

#endif

#if defined(_WIN32)
#if !defined(_WIN32_WINNT)
// The following Windows API function is declared explicitly;
// otherwise any user would have to specify /D_WIN32_WINNT=0x0400
extern "C" BOOL WINAPI TryEnterCriticalSection( LPCRITICAL_SECTION );
#endif
#endif


#ifndef STDERR_DEFINED
#define STDERR_DEFINED
#define STDERRSEP()      std::cerr << "---------------------------" << std::endl
#define STDERR(a)        std::cerr << #a ": "<<(a)<<std::endl
#define STDERR2(a,b)     std::cerr << #a ": "<<(a)<<"    "<< #b ": "<<(b)<<std::endl
#define STDERR3(a,b,c)   std::cerr << #a ": "<<(a)<<"    "<< #b ": "<<(b)<<"    "<< #c ": "<<(c)<<std::endl
#define STDERR4(a,b,c,d) std::cerr << #a ": "<<(a)<<"    "<< #b ": "<<(b)<<"    "<< #c ": "<<(c)<<"    "<< #d ": "<<(d)<<std::endl
#define STDERR5(a,b,c,d,e) std::cerr << #a ": "<<(a)<<"    "<< #b ": "<<(b)<<"    "<< #c ": "<<(c)<<"    "<< #d ": "<<(d)<<"    "<< #e ": "<<(e)<<std::endl
#define STDL() std::cerr << std::string(__FILE__) << ":" << int(__LINE__) << std::endl
#define STDERRDUMP(a) a.Dump(#a)
#endif

namespace Easy
{


//-----------------------------------------------------------------------------------
/// Generic singleton class.
///
template <typename T>
class Singleton
{
private:
    Singleton(const Singleton<T> &);
    Singleton& operator=(const Singleton<T> &);

protected:
    static T* _singleton;

public:
    Singleton( void )
    {
        assert( !_singleton );
#if defined( _MSC_VER ) && _MSC_VER < 1200
        int offset = (int)(T*)1 - (int)(Singleton <T>*)(T*)1;
        _singleton = (T*)((int)this + offset);
#else
        _singleton = static_cast< T* >( this );
#endif
    }

    ~Singleton( void )
    {
        assert( _singleton );
        _singleton = 0;
    }

    static T& getSingleton( void )
    {
        assert(_singleton);
        return (*_singleton);
    }
};

//-----------------------------------------------------------------------------------
class StringArray : public std::vector<std::string>
{
public:
    StringArray();
    StringArray(const char* s, const char* seperators=",");
    //StringArray(const StringArray &sl);
    StringArray(const std::string &text, const std::string &seperators=",");

    void set(const std::string &text, const std::string &seperators=",");

    bool contains(const std::string& text);
};

//-----------------------------------------------------------------------------------
bool isFile(const std::string& path);

//-----------------------------------------------------------------------------------
class Timer
{
public:
    double msInvFrequency;

#if defined(_WIN32)
    mutable double mLastQueryTime;

    Timer()
    {
		msInvFrequency = -1.0;
        reset();
    }

    void reset()
    {
		if (msInvFrequency<=0)
		{
			LARGE_INTEGER lFreq;
			QueryPerformanceFrequency(&lFreq);
			msInvFrequency = 1.0 / double(lFreq.QuadPart);
		}
		LARGE_INTEGER lCount;
		QueryPerformanceCounter(&lCount);
		mLastQueryTime = (lCount.QuadPart * msInvFrequency);
    }

    float value()
    {
		double lastTime = mLastQueryTime;
		LARGE_INTEGER lCount;
		QueryPerformanceCounter(&lCount);
		return (float)((lCount.QuadPart * msInvFrequency) - lastTime);
    }

#else
    long start_sec;
    long start_usec;

    Timer()
    {
        reset();
    }

    void reset()
    {
        struct timeval tv;
        struct timezone tz;
        gettimeofday(&tv,&tz);
        start_sec=tv.tv_sec;
        start_usec=tv.tv_usec;
    }

    float value()
    {
        struct timeval tv;
        struct timezone tz;
        gettimeofday(&tv,&tz);
        return((tv.tv_sec-start_sec)+(tv.tv_usec-start_usec)/1000000.0);
    }
#endif
};

/// class to construct printf-formating with std::string
class Stringf : public std::string
{
public:
    Stringf(const char *format, ...);
    Stringf(const std::string& format, ...);
};

/// return a random number between 0 and 1.
inline float frand()
{
    return rand() / (float) RAND_MAX;
}

/// return a random number between -1 and +1.
inline float sfrand()
{
    return (rand() / (float) RAND_MAX)*2.0f-1.0f;
}

void promptForKey();

//----------------------------------------------------------------------------------------------
#if !defined(_WIN32)

inline int __nsleep(const struct timespec *req, struct timespec *rem)
{
    struct timespec temp_rem;
    if(nanosleep(req,rem)==-1)
        return __nsleep(rem, &temp_rem);
    else
        return 1;
}
//----------------------------------------------------------------------------------------------
inline int msleep(unsigned long msec)
{
    struct timespec req= {0,0},rem= {0,0};
    time_t sec=(int)(msec/1000);
    msec=msec-(sec*1000);
    req.tv_sec=sec;
    req.tv_nsec=msec*1000000L;
    __nsleep(&req,&rem);
    return 1;
}

#endif

//----------------------------------------------------------------------------------------------
class Thread
{
    friend void *func(void *t);

#if defined(_WIN32)
    HANDLE _thread;
#else
    pthread_t _thread;
#endif

public:
    int _returnCode;
    bool _isComplete;

public:
    Thread();
    virtual ~Thread();

    /// Create and run.
    bool execute();

    /// Execute the work of the task.
    virtual void onExecute() = 0;

public:
    /// Only sends the message.
    void kill();

    /// Sends the message and then waits.
    void killWait();

    /// Waits until it has finished.
    void wait();

    /// Returns whether it has completed without interruption.
    bool isCompleted();
};

//----------------------------------------------------------------------------------------------
class Mutex
{
private:
#if defined(_WIN32)
    typedef CRITICAL_SECTION MUTEX_TYPE;
#else
    typedef pthread_mutex_t MUTEX_TYPE;
#endif
    MUTEX_TYPE mutex;

public:
    Mutex()
    {
#if defined(_WIN32)
        //mutex = CreateMutex(NULL,FALSE,NULL);
        InitializeCriticalSection(&mutex);
#else
        pthread_mutex_init(&mutex,NULL);
#endif
    }

    ~Mutex()
    {
#if defined(_WIN32)
        //CloseHandle(mutex);
        DeleteCriticalSection(&mutex);
#else
        pthread_mutex_destroy(&mutex);
#endif
    }

    void claim()
    {
#if defined(_WIN32)
        //DWORD dwWaitResult = WaitForSingleObject(mutex,INFINITE);
        EnterCriticalSection(&mutex);
#else
        pthread_mutex_lock(&mutex);
#endif
    }

    bool tryClaim()
    {
#if defined(_WIN32)
        //return(WaitForSingleObject(mutex,0)==WAIT_OBJECT_0);
        return (TryEnterCriticalSection(&mutex)!=0);
#else
        return(pthread_mutex_trylock(&mutex)==0);
#endif
    }

    void release()
    {
#if defined(_WIN32)
        //ReleaseMutex(mutex);
        LeaveCriticalSection(&mutex);
#else
        pthread_mutex_unlock(&mutex);
#endif
    }

    bool isLocked()
    {
#if defined(_WIN32)
        //return(WaitForSingleObject(mutex,0)==WAIT_TIMEOUT);
        return (TryEnterCriticalSection(&mutex)==0);
#else
        return(pthread_mutex_trylock(&mutex)==EBUSY);
#endif
    }

    MUTEX_TYPE &asNative()
    {
        return mutex;
    }
};

//----------------------------------------------------------------------------------------------
class ScopedMutex
{
    Mutex& _mutex;
public:
    ScopedMutex(Mutex& mutex)
        :
        _mutex(mutex)
    {
        _mutex.claim();
    }

    ~ScopedMutex()
    {
        _mutex.release();
    }
};

//----------------------------------------------------------------------------------------------
#if defined(_WIN32)
#else
void Semaphore_Init();
#define ustestsema(x) (x.__align)
#endif

class Semaphore
{
private:
#if defined(_WIN32)
    unsigned int volatile count;
    HANDLE sema;
#else
    sem_t sema;
#endif

public:
    Semaphore(int _count=1)
    {
#if defined(_WIN32)
        count = (_count);
        sema = CreateSemaphore(
                   NULL,           // default security attributes
                   count,  // initial count
                   100,  // maximum count
                   NULL);          // unnamed semaphore
#else
        int rc=sem_init(&sema,0,_count);
        assert(rc==0);
#endif
    }

    ~Semaphore()
    {
#if defined(_WIN32)
        CloseHandle(sema);
#else
        int rc=sem_destroy(&sema);
        assert(rc==0);
#endif
    }

    inline void wait()
    {
        claim();
    }

    inline void signal()
    {
        release();
    }

    void claim()
    {
        //fprintf(stderr,"Claiming %p %d\n",this,ustestsema(sema));
#if defined(_WIN32)
        if (!tryClaim(INFINITE))
            perror("Semaphore: wait failed");
#else
        int rc=0;
        do
        {
            rc = sem_wait(&sema);
        }
        while(rc==-1 && errno==EINTR);

        if(rc != 0)
            perror("sem_wait failed");
#endif
        //fprintf(stderr,"Claimed %p %d\n",this,ustestsema(sema));
    }

    bool tryClaim(unsigned int timeoutms=0L)
    {
        //fprintf(stderr,"Claiming %p %d\n",this,ustestsema(sema));
#if defined(_WIN32)
        DWORD dwMilliseconds = DWORD(timeoutms);
        switch (WaitForSingleObject(sema, dwMilliseconds))
        {
        case WAIT_OBJECT_0:
            InterlockedDecrement((LONG*)&count);
            return true;

        case WAIT_TIMEOUT:
            return false;

        default:
            perror("Semaphore: WaitForSingleObject failed");
            return false;
        }
        //return true;
#else
        //return(sem_trywait(&sema)==0);
        if (timeoutms == 0)
            return (sem_trywait(&sema) == 0);

        Timer timer;
        timeoutms += (unsigned int)(timer.value()*1000.f);
        do
        {
            int rc = sem_trywait(&sema);
            if (rc == 0)
                return true;
            msleep(1);
        }
        while ( (unsigned int)(timer.value()*1000.f) < timeoutms );
        return false;
#endif
        //fprintf(stderr,"Claimed %p %d\n",this,ustestsema(sema));
    }

    void release()
    {
        //fprintf(stderr,"Releasing %p %d\n",this,ustestsema(sema));
#if defined(_WIN32)
        InterlockedIncrement((LONG*)&count);
        if ( ReleaseSemaphore(sema, 1, NULL) == FALSE )
        {
            InterlockedDecrement((LONG*)&count);
            perror("Semaphore: post failed");
        }

#else
        int rc=sem_post(&sema);
        assert(rc==0);
#endif
        //fprintf(stderr,"Released %p %d\n",this,ustestsema(sema));
    }

    int value()
    {
#if defined(_WIN32)
        return count;
#else
        int v;
        sem_getvalue(&sema,&v);
        return(v);
#endif
    }
};

//----------------------------------------------------------------------------------------------
}

#endif // STD_UTILS_H_INCLUDED
