/*
 * Copyright 1993-2012 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

#include "interop.h"

using namespace Easy;

// a global timer.
static Timer timer;

//---------------------------------------------------------------------------------------------------
/// Benchmark just the CUDA kernel alone.
///
float benchmark_CUDA(int nTimingIterations)
{
    void* d_buffer = 0;

    glFinish();

    CUDA_CHECK(cudaMalloc(&d_buffer, g_copySize));
    CUDA_CHECK(cudaDeviceSynchronize());

	nvtxRangePush("cuda-only");
	timer.reset();
	
	//GPU based timing is commented out but can be used for timing as well.
	//CUDA_CHECK(cudaEventRecord(cudaTaskStart, cudaStream));

	for (int iter=0; iter<nTimingIterations; ++iter)
    {
		doWorkInCUDA(iter, int(g_nDelayIterations_CUDA), d_buffer, g_copySize, 0);
        CUDA_CHECK(cudaStreamSynchronize(0));
    }

	//CUDA_CHECK(cudaEventRecord(cudaTaskEnd, cudaStream));
    //CUDA_CHECK(cudaEventSynchronize(cudaTaskEnd));
    //float msecCUDA;
	//CUDA_CHECK(cudaEventElapsedTime(&msecCUDA, cudaTaskStart, cudaTaskEnd));
	//msecCUDA /= g_numIterations;
	
	CUDA_CHECK(cudaStreamSynchronize(0));

	float elapsedMs = timer.value()*1000;
    nvtxRangePop();

    CUDA_CHECK(cudaFree(d_buffer));

	elapsedMs /= nTimingIterations;
	return elapsedMs;
}

//---------------------------------------------------------------------------------------------------
/// Benchmark the copy.
///
float benchmark_Copy(int nTimingIterations)
{
    // cudaDeviceSynchronize is necessary call to make when doing CPU based timings to 
    // ensure that all the preceding CUDA commands are done before we start timing.
	CUDA_CHECK(cudaDeviceSynchronize());

    nvtxRangePush("copy-only");
    timer.reset();

    int numBytes = int(g_nDelayIterations_Copy);
    
    void* d_buffer;
    void* h_buffer;
    CUDA_CHECK(cudaMalloc(&d_buffer, numBytes));
    CUDA_CHECK(cudaHostAlloc(&h_buffer, numBytes, 0));

	for (int iter=0; iter<nTimingIterations; ++iter) 
    {
        cudaMemcpyAsync(h_buffer, d_buffer, numBytes, cudaMemcpyDeviceToHost, 0);
    }

	CUDA_CHECK(cudaDeviceSynchronize());

	float elapsedMs = timer.value()*1000;
	nvtxRangePop();

    CUDA_CHECK(cudaFree(d_buffer));
    CUDA_CHECK(cudaFreeHost(h_buffer));

	elapsedMs /= nTimingIterations;
    return elapsedMs;
}

//---------------------------------------------------------------------------------------------------
/// Benchmark just the GL rendering.
///
float benchmark_GL(int nTimingIterations)
{
    // (GPU based timing is commented out but can be used for timing instead.)
	glFinish();

    nvtxRangePush("gl-only");
    timer.reset();
	
	//glBeginQuery(GL_TIME_ELAPSED_EXT, g_glTimerQuery);	
    //GL_CHECK_ERRORS();

    for (int iter=0; iter<nTimingIterations; ++iter) 
    {
        doRender(iter, g_dataGLId[0]);
    }

	//glEndQuery(GL_TIME_ELAPSED_EXT);
	//GL_CHECK_ERRORS();
	
    // glFinish is necessary call to make when timing to ensure that all the 
    // preceding OpenGL commands are done before we get the time.
    glFinish();    

    float elapsedMs = timer.value()*1000;
    nvtxRangePop();

    // get timing info...
	//GLuint64EXT timeElapsed = 0;
	//glGetQueryObjectui64vEXT(g_glTimerQuery, GL_QUERY_RESULT, &timeElapsed);
	//GL_CHECK_ERRORS(); 
	//float msecGL = timeElapsed*0.000001;
	//msecGL /= g_numIterations;

    // clear the screen so we don't have junk on it.
    glClear(GL_COLOR_BUFFER_BIT);
    glFinish();

	elapsedMs /= nTimingIterations;
    return elapsedMs;
}

//---------------------------------------------------------------------------------------------------
/// Benchmark CUDA and GL, with data shared through interop
///
/// We must use CPU based timing here since there is a mix of APIs with interdependant calls that
/// are possibly not CPU asynchronous.
///

//---------------------------------------------------------------------------------------------------
bool benchmarkInterop_begin()
{
    // glFinish is necessary call to make when doing CPU based timings to ensure 
    // that all the preceding OpenGL commands are done before we start timing.
	glFinish();

    // cudaStreamSynchronize is necessary call to make when doing CPU based timings to 
    // ensure that all the preceding CUDA commands are done before we start timing.
	CUDA_CHECK(cudaDeviceSynchronize());

    timer.reset();

    glQueryCounter(g_glTimeStampStart, GL_TIMESTAMP);
    glFlush();

    return true;
}

//---------------------------------------------------------------------------------------------------
float benchmarkInterop_end(int nTimingIterations)
{
	// glFinish is necessary call to make when doing CPU based timings to make
	// sure that all the preceding OpenGL commands are done before we finish timing.
	glFinish();

	// cudaStreamSynchronize is necessary call to make when doing CPU based timings to make
	// sure that all the preceding CUDA commands are done before we finish timing.
	CUDA_CHECK(cudaDeviceSynchronize());

    float elapsedMs = timer.value()*1000;

    if (nTimingIterations > 0)
	    elapsedMs /= nTimingIterations;

	return elapsedMs;
}

//---------------------------------------------------------------------------------------------------
float benchmark_Interop_WriteDiscard(int nTimingIterations)
{
    cudaStream_t stream;
    CUDA_CHECK(cudaStreamCreate(&stream));

    // register as WRITE_DISCARD.
    registerResources(false, true);

    benchmarkInterop_begin();

    nvtxRangePush("driver(WD)");

    int iter = 0;
    for (; iter<nTimingIterations; ++iter) 
    {
        void* d_mappedPtr;
        doMap(&d_mappedPtr, stream);
        
        doKernel(iter, d_mappedPtr, stream);

        doUnmap(stream);

        doRender(iter, g_dataGLId[0]);

        glFlush();
    }

	float ms = benchmarkInterop_end(iter);

    nvtxRangePop();
    unregisterResources();

    CUDA_CHECK(cudaStreamDestroy(stream));

    return ms;
}

//---------------------------------------------------------------------------------------------------
float benchmark_Interop(int nTimingIterations)
{
    cudaStream_t stream;
    CUDA_CHECK(cudaStreamCreate(&stream));

    registerResources(true, true);

    benchmarkInterop_begin();

    nvtxRangePush("driver");
    
    int iter = 0;
    for (; iter<nTimingIterations; ++iter) 
    {
        void* d_mappedPtr;
        doMap(&d_mappedPtr, stream);
        
        doKernel(iter, d_mappedPtr, stream);

        doUnmap(stream);

        doRender(iter, g_dataGLId[0]);

        glFlush();
    }

	float ms = benchmarkInterop_end(iter);

    nvtxRangePop();
    unregisterResources();

    CUDA_CHECK(cudaStreamDestroy(stream));

    return ms;
}

//---------------------------------------------------------------------------------------------------