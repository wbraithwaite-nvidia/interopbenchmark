/*
 * Copyright 1993-2012 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

#include "interop.h"

using namespace Easy;

//---------------------------------------------------------------------------------------------------
/// Benchmark CUDA and GL, with data shared through interop
/// but this is done manually by having a cuda context on the display gpu.
///
/// We must use CPU based timing here since there is a mix of APIs with interdependant calls that
/// are possibly not CPU asynchronous.
///
static float benchmark_Manual_WriteDiscard_Async(int nTimingIterations, bool useStreams, bool useEvents)
{
    CUDA_CHECK(cudaSetDevice(g_cudaDeviceIndex));
	CUDA_CHECK(cudaFree(0));

    cudaStream_t stream;
    CUDA_CHECK(cudaStreamCreate(&stream));
    cudaStream_t copyStream = stream;

    cudaEvent_t kernelEventComplete;
    cudaEvent_t copyEventCudaToHostComplete;
    cudaEvent_t copyEventHostToCudaComplete;
    CUDA_CHECK(cudaEventCreate(&kernelEventComplete));
    CUDA_CHECK(cudaEventCreate(&copyEventCudaToHostComplete));
    CUDA_CHECK(cudaEventCreate(&copyEventHostToCudaComplete));

    // create the references on the display GPU...
    CUDA_CHECK(cudaSetDevice(g_glDeviceIndex));
	CUDA_CHECK(cudaFree(0));
    registerResources(false, true);
    cudaStream_t copyStreamGl;
    CUDA_CHECK(cudaStreamCreate(&copyStreamGl));
    cudaEvent_t copyEventHostToGlComplete;
    cudaEvent_t copyEventGlToHostComplete;
    CUDA_CHECK(cudaEventCreate(&copyEventHostToGlComplete));
    CUDA_CHECK(cudaEventCreate(&copyEventGlToHostComplete));
    CUDA_CHECK(cudaSetDevice(g_cudaDeviceIndex));

    void* d_buffer[2] = {0,0};
    CUDA_CHECK(cudaMalloc(&d_buffer[0], g_copySize));
    void* h_dataPtr[2] = {0,0};
    CUDA_CHECK(cudaHostAlloc((void**)&h_dataPtr[0], g_copySize, cudaHostAllocPortable));//|cudaHostAllocWriteCombined));

    // glFinish is necessary call to make when doing CPU based timings to ensure 
    // that all the preceding OpenGL commands are done before we start timing.
	glFinish();

    // cudaStreamSynchronize is necessary call to make when doing CPU based timings to 
    // ensure that all the preceding CUDA commands are done before we start timing.
	CUDA_CHECK(cudaDeviceSynchronize());

    nvtxRangePush("manual-wil");
    Timer timer;

    glQueryCounter(g_glTimeStampStart, GL_TIMESTAMP);
    glFlush();

    for (int iter=0; iter<nTimingIterations; ++iter) 
    {
        doKernel(iter, d_buffer[0], stream);

        if (useEvents)
        {
            /// CAVEAT: THIS IS REQUIRED OR WE DRIFT OUT OF SYNC!
            // wait for gl to finish copying the data from the host.
            CUDA_CHECK(cudaStreamWaitEvent(stream, copyEventHostToGlComplete, 0));
        }

        CUDA_CHECK(cudaMemcpyAsync(h_dataPtr[0], d_buffer[0], g_copySize, cudaMemcpyDeviceToHost, stream));
        //CUDA_CHECK(cudaEventRecord(copyEventCudaToHostComplete, stream));
        CUDA_CHECK(cudaStreamSynchronize(stream));

        //----------------------------------------------------------------------------
        // now use the cuda context on the display GPU...

        CUDA_CHECK(cudaSetDevice(g_glDeviceIndex));

        void* d_mappedPtr;
        doMap((void**)&d_mappedPtr, copyStreamGl);

        //CUDA_CHECK(cudaStreamWaitEvent(copyStreamGl, copyEventCudaToHostComplete, 0));
        if (useStreams)
        {
            CUDA_CHECK(cudaMemcpyAsync(d_mappedPtr, h_dataPtr[0], g_copySize, cudaMemcpyHostToDevice, copyStreamGl));
            if (useEvents)
                CUDA_CHECK(cudaEventRecord(copyEventHostToGlComplete, copyStreamGl));
        }
        else
        {
            CUDA_CHECK(cudaMemcpy(d_mappedPtr, h_dataPtr[0], g_copySize, cudaMemcpyHostToDevice));
        }

        doUnmap(copyStreamGl);

        /// CAVEAT: it is important that we do NOT sync here, 
        /// or the next kernel can't start until the HtoGL upload is finished!
        /// SO DO NOT CALL: CUDA_CHECK(cudaStreamSynchronize(copyStreamGl));

        CUDA_CHECK(cudaSetDevice(g_cudaDeviceIndex));
        //----------------------------------------------------------------------------

        doRender(iter, g_dataGLId[0]);

        glFlush();
    }

	// glFinish is necessary call to make when doing CPU based timings to make
	// sure that all the preceding OpenGL commands are done before we finish timing.
	glFinish();

	// cudaStreamSynchronize is necessary call to make when doing CPU based timings to make
	// sure that all the preceding CUDA commands are done before we finish timing.
	CUDA_CHECK(cudaDeviceSynchronize());

    float elapsedMsec = timer.value()*1000;
    nvtxRangePop();

    CUDA_CHECK(cudaEventDestroy(kernelEventComplete));
    CUDA_CHECK(cudaEventDestroy(copyEventCudaToHostComplete));
    CUDA_CHECK(cudaEventDestroy(copyEventHostToCudaComplete));
    if (copyStream != stream)
        CUDA_CHECK(cudaStreamDestroy(copyStream));

    CUDA_CHECK(cudaSetDevice(g_glDeviceIndex));
    unregisterResources();
    CUDA_CHECK(cudaStreamDestroy(copyStreamGl));
    //cudaEventDestroy(kernelEventComplete);
    //cudaEventDestroy(copyEventCudaToHostComplete);
    //cudaEventDestroy(copyEventHostToCudaComplete);
    CUDA_CHECK(cudaEventDestroy(copyEventHostToGlComplete));
    CUDA_CHECK(cudaEventDestroy(copyEventGlToHostComplete));
    
    CUDA_CHECK(cudaSetDevice(g_cudaDeviceIndex));
    CUDA_CHECK(cudaStreamDestroy(stream));
    CUDA_CHECK(cudaFreeHost(h_dataPtr[0]));
    CUDA_CHECK(cudaFree(d_buffer[0]));

	elapsedMsec /= nTimingIterations;
	return elapsedMsec;
}

//---------------------------------------------------------------------------------------------------
float benchmark_Manual_WriteDiscard(int nTimingIterations)
{
    return benchmark_Manual_WriteDiscard_Async(nTimingIterations, false, false);
}

//---------------------------------------------------------------------------------------------------
float benchmark_Manual_WriteDiscard_Async(int nTimingIterations)
{
    return benchmark_Manual_WriteDiscard_Async(nTimingIterations, true, true);
}

//---------------------------------------------------------------------------------------------------
