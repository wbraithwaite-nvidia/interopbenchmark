/*
 * Copyright 1993-2012 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

/** 
 This sample is designed to test CUDA-OpenGL interoperability performance in a system.
- Requires CUDA version 5.0 or greater.

- It creates a GLUT window and tests mapping/unmapping of a texture of a given size
  using GL/CUDA interoperability on every CUDA device available in the system.

- The application computes "speedup" for each CUDA/GL context configuration possible in a given system.
  The speedup is the frame rate compared to pure workload(no interoperability) framerate 

- The application also auto-adjusts the time it spends in GL work and CUDA kernels
  so that it simulates a specified workload.  This is because the number of iterations 
  is sensitive to compiler changes, clocks, GPU type, etc.
 **/

#include "gl_utils.h"
#include "std_utils.h"
#include "utils.h"

#include <GL/freeglut.h>
#include <cuda_runtime_api.h>
#include <cuda_gl_interop.h>
#include <cuda_profiler_api.h>
#include <cassert>
#include <cmath>

#include "interop.h"
#include "stb_image.h"

using namespace Easy;

//---------------------------------------------------------------------------------------------------
/// arguments for the benchmarking app...
///
// don't open an interactive glut window.
bool g_useBatchMode = false;
// print the GL timestamps.
bool g_printGlTiming = false;
// manually wait for OpenGL to finish, rather than letting the CUDA driver do it.
bool g_useGLsync = false; 
// hide the GL window so it doesn't obscure the view.
bool g_hideWindow = false;
// not used.
bool g_usePingPong = false;
// wait for a key on completion. (useful when running from visual studio)
bool g_waitForKey = false;
// number fo iterations to get the timing information over.
int g_numIterations = 20;
// synchronize the cuda work so we can see it in the profiler.
bool g_useCudaSync = false; 
// The time to spend doing CUDA work.
// - this allows you to simulate different amounts of CUDA work being done
//   per frame (the default is fully-loaded at ~60 fps).
// - the application will spend this many msec running CUDA work.
// - this is used to compute g_nDelayIterations_CUDA.
float g_targetMsecCUDA = 20.f;
// The time to spend doing OpenGL work.
// - this allows you to simulate different amounts of OpenGL rendering being done
//   per frame (the default is fully-loaded at ~60 fps)
// - the application will spend this many msec running OpenGL rendering
// - this is used to compute g_nDelayIterations_GL
float g_targetMsecGL = 10.f;
// The time to spend doing the copy.
// - the application will spend this many msec transferring data 
// - this is used to compute g_nDelayIterations_Copy
// - we default to using g_copySize instead.
float g_targetMsecCopy = -1.f;
// This is used if the copy work duration isn't specified.
unsigned int g_copySize = 20*1024*1024;

//---------------------------------------------------------------------------------------------------

// window dimensions.
int g_windowWidth = 800;
int g_windowHeight = 600;

// Data used by GL and CUDA kernels...
#if 0
HPBUFFERARB  pbufferHandle;
#endif
GlContextData glContext;
GlContextData windowContext;
cudaGraphicsResource_t g_dataRes[2] = {0};
GLuint g_dataGLId[2] = {0};
int g_texWidth = 0;
int g_texHeight = 0;

// The current device ordinals.
int g_cudaDeviceIndex = 0;
int g_glDeviceIndex = 0;
std::string g_cudaDeviceName;
std::string g_glDeviceName;

// num iterations needed to reach target durations...
float g_nDelayIterations_CUDA = 64*1024; // 8860, 14771
float g_nDelayIterations_GL = 64*1024;
float g_nDelayIterations_Copy = 64*1024; // (this actual maps to the numBytes for g_copySize)

GLuint g_glTimeStampStart;
GLuint* g_glTimeStamps;

//---------------------------------------------------------------------------------------------------
/// local variables
///
static float g_actualMs_GL = -1;
static float g_actualMs_CUDA = -1;
static float g_actualMs_Copy = -1;

static std::map<std::string, float> g_prevElapsedTimes;
static std::map<std::string, std::string> g_prevMethodNames;

static bool initializedWorkConfig = false;
static int g_cudaDeviceCount = 0;
static unsigned int g_glDeviceCount = 16;
static int g_glDeviceIndices[16];

static nvtxEventAttributes_t g_eventAttrib;

static bool g_useWriteDiscard = false;
static bool g_useReadOnly = false;

struct Image
{
    int width, height;
    int numComponents;
    unsigned char* pixels;
};

static Image image = {0,0,0,0};

//---------------------------------------------------------------------------------------------------
/// external variables
///
extern GLsync g_glSyncId;
extern GLuint g_displayTexId;

//---------------------------------------------------------------------------------------------------
/// Function declarations...
///
void drawHUD();

void deleteData_CUDA();
void allocateData_CUDA(int numBytes);
void deleteData_GL();
void allocateData_GL(int numBytes);

void printHotkeys();
void printHelp();
void printGlTiming();

void setWorkConfig(float cudaMs, float glMs, float copySize, float copyMs=-1, bool forceTuning=false);
void setDeviceConfig(int cudaDevice);

void initGL();
void cleanupGL();

void runBenchmark(std::string name, BenchmarkFunc func, int nTimingIterations);
void startBenchmark(int i);
void runAllBenchmarks(int nTimingIterations);

// glut callbacks.
void onDisplay();
void onReshape(int w, int h);
void onKeyboard(unsigned char key, int x, int y);

//---------------------------------------------------------------------------------------------------
/// Glut callback
///
void onDisplay()
{
    // do nothing.
    /// CAVEAT: we need a glutDisplayFunc to ensure the glutReshapeFunc actually fires its callback!
}

//---------------------------------------------------------------------------------------------------
/// Glut callback
///
void onReshape(int w, int h)
{
    startGlContext(&windowContext);

    // update the global window dimensions.
    g_windowWidth = w;
    g_windowHeight = h;

    // setup the viewport and matrices...
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, w, h-1, 0, -1, 1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

    glClear(GL_COLOR_BUFFER_BIT);
    if (initializedWorkConfig)
        drawLogo(g_numIterations-1);
    drawHUD();    
}

//---------------------------------------------------------------------------------------------------
/// Helper function for printing the timing result.
///
void printFormattedResult(const std::string& name, float ms)
{
    printf("    %s", name.c_str());
    for (int i=48-int(name.length()); i>0; --i)
        printf(" ");
    printf("= ");
    printf("%5.2f msec/frame", ms);
}

//---------------------------------------------------------------------------------------------------
/// Run a specific benchmarking function.
///
void runBenchmark(std::string name, BenchmarkFunc func, int nTimingIterations)
{
    startGlContext(&glContext);

    float ms = func(nTimingIterations);

    printFormattedResult(name, ms);

	float speedup = (g_actualMs_CUDA + g_actualMs_GL) / ms;
	printf("    Speedup (vs. pure workload): x%.2f\n", speedup);

    printGlTiming();

    g_prevElapsedTimes[name] = ms;
    g_prevMethodNames[name] = name;

    startGlContext(&windowContext);
    /*
    static GLuint pbufferTexture=0;
    if(pbufferTexture == 0)
        glGenTextures(1, &pbufferTexture);
    glBindTexture(GL_TEXTURE_2D, pbufferTexture);
	//use the pbuffer as the texture
	wglBindTexImageARB(pbufferHandle, WGL_FRONT_LEFT_ARB);

	//Draw simple rectangle
	glBegin(GL_TRIANGLE_STRIP);
	{
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(0.0f,  100.0f, 0.0f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f( 100.0f, 0.0f, 0.0f);
		glTexCoord2f(1.0f, 1.0f);
		glVertex3f( 100.0f,  100.0f, 0.0f);
	}
	glEnd();

	//release the pbuffer for further rendering
	wglReleaseTexImageARB(pbufferHandle, WGL_FRONT_LEFT_ARB);
    glBindTexture(GL_TEXTURE_2D, 0);
    */
    glClear(GL_COLOR_BUFFER_BIT);
    drawLogo(nTimingIterations-1);
    drawHUD();

    glutSwapBuffers();
}

//---------------------------------------------------------------------------------------------------
void startBenchmark(int i)
{
    switch(i)
    {
    case 1:
        runBenchmark("1. driver", benchmark_Interop, g_numIterations); 
        break;
    case 2:
        runBenchmark("2. driver (WRITE-DISCARD)", benchmark_Interop_WriteDiscard, g_numIterations);
        break;
    case 3:
        runBenchmark("3. manual (WRITE-DISCARD)", benchmark_Manual_WriteDiscard, g_numIterations);
        break;
    case 4:
        runBenchmark("4. manual (WRITE-DISCARD, Async)", benchmark_Manual_WriteDiscard_Async, g_numIterations);
        break;
    case 5:
        runBenchmark("5. manual (WRITE-DISCARD, Async, Ping-Pong)", benchmark_Manual_WriteDiscard_Async_PingPong, g_numIterations);
        break;
    case 6:
        runBenchmark("6. manual (WRITE-DISCARD, Async, Peer-to-Peer)", benchmark_Manual_WriteDiscard_Async_P2P, g_numIterations);
        break;
    case 7:
    case 8:
    case 9:
        break;
    }
}

//---------------------------------------------------------------------------------------------------
void runAllBenchmarks(int nTimingIterations)
{
    printf("Benchmarks:\n");

    {
        printFormattedResult("GL only", g_actualMs_GL);
        printf("\n");
    }
    
    {
        printFormattedResult("CUDA only", g_actualMs_CUDA);
        printf("\n");
    }
    
    {
        printFormattedResult("Copy only", g_actualMs_Copy);
        printf("\n");
    }

    // run all ten benchmarks. 
    // NB. they might not exist. Check the startBenchmark function.
    for (int i=0; i<=9; ++i)
    {
        startBenchmark(i);
    }
}

//---------------------------------------------------------------------------------------------------
void onKeyboard(unsigned char key, int /*x*/, int /*y*/)
{
    if (key == 27 || key == 'q')
    {
        exit(0);
    }
    else if (key == 'r')
    {
        runAllBenchmarks(g_numIterations);
    }
    else if (key >= '1' && key <='9')
    {
        int i = int(key)-'1'+1;
        printf("Running benchmark[%d]...\n", i);
        startBenchmark(i);
    }
    else if (key == 'd')
    {
        // cycle through the device configs.
        g_cudaDeviceIndex = (g_cudaDeviceIndex+1)%g_cudaDeviceCount;
        setDeviceConfig(g_cudaDeviceIndex);
    }
    else if (key == '=' || key == '+')
    {
        setWorkConfig(g_targetMsecCUDA, g_targetMsecGL+5.f, float(g_copySize)); 
    }
    else if (key == '-')
    {
        setWorkConfig(g_targetMsecCUDA, g_targetMsecGL-5.f, float(g_copySize)); 
    }
    else if (key == ']')
    {
        setWorkConfig(g_targetMsecCUDA+5.f, g_targetMsecGL, float(g_copySize)); 
    }
    else if (key == '[')
    {
        setWorkConfig(g_targetMsecCUDA-5.f, g_targetMsecGL, float(g_copySize)); 
    }
    else if (key == '\'')
    {
        setWorkConfig(g_targetMsecCUDA, g_targetMsecGL, float(g_copySize)+5.f*1024*1024); 
    }
    else if (key == ';')
    {
        setWorkConfig(g_targetMsecCUDA, g_targetMsecGL, float(g_copySize)-5.f*1024*1024); 
    }
    else if (key == 'h')
    {
        printHotkeys();
    }

    else
    {
        //STDERR(int(key));
    }
}

//---------------------------------------------------------------------------------------------------
void drawHUD()
{
    if (g_hideWindow)
        return;

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glColor4f(1,1,1,1);

    const int FONT_HEIGHT = 15;
    const int LINE_SPACING = 24;
    const int PADDING = 10;

    float sx = PADDING, sy = FONT_HEIGHT + PADDING;

    drawGlText(sx, sy, Stringf("render-GPU = %d (%s)  /  compute-GPU = %d (%s)", g_glDeviceIndex, g_glDeviceName.c_str(), g_cudaDeviceIndex, g_cudaDeviceName.c_str()).c_str());
    sy += LINE_SPACING;
    sy += LINE_SPACING;
    drawGlText(sx, sy, Stringf("GL duration   = %1.2f ms", g_actualMs_GL).c_str());
    sy += LINE_SPACING;
    drawGlText(sx, sy, Stringf("CUDA duration = %1.2f ms", g_actualMs_CUDA).c_str());
    sy += LINE_SPACING;
    drawGlText(sx, sy, Stringf("Copy duration = ~%1.2f ms (%1.2f MB)", g_actualMs_Copy, float(g_copySize)/(1024*1024)).c_str());
    sy += LINE_SPACING;
    sy += LINE_SPACING;

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    if (g_prevMethodNames.size() > 0)
    {
        glColor4f(0.1f,0.1f,0.1f,0.75f);
        drawGlRectangle(sx-4.f, sy-LINE_SPACING, g_windowWidth-PADDING+4.f, sy + LINE_SPACING*(g_prevMethodNames.size()) - (LINE_SPACING-PADDING));
        glColor4f(1,1,1,1);

        std::map<std::string, std::string>::const_iterator it = g_prevMethodNames.begin();
        for (; it != g_prevMethodNames.end(); ++it)
        {
            float ms = g_prevElapsedTimes[it->first];
            float relerr = 1 - (ms / (g_actualMs_GL + g_actualMs_CUDA));

            if (fabs(relerr) < 0.1) // if within 10% then consider it even-ish.
                glColor4f(1,1,0,1);
            else if (relerr > 0) // otherwise, if we are faster
                glColor4f(0,1,0,1);
            else if (relerr < 1) // or if we are slower
                glColor4f(1,0,0,1);

            drawGlText(sx, sy, Stringf("%s", it->first.c_str()).c_str());
            drawGlText(sx+g_windowWidth-200, sy, Stringf("= %1.2f ms", ms).c_str());
            sy += LINE_SPACING;
        }

        glColor4f(1,1,1,1);
    }
    else
    {
        glColor4f(0.1f,0.1f,0.1f,0.75f);
        drawGlRectangle(sx-4.f, sy-LINE_SPACING, g_windowWidth-PADDING+4.f, sy + LINE_SPACING*(1) - (LINE_SPACING-PADDING));
        glColor4f(1,1,0,1);
        drawGlText(sx, sy, "Press 'R' to run all benchmarks."); sy += LINE_SPACING;
    }

    glDisable(GL_BLEND);
}

//---------------------------------------------------------------------------------------------------
void setBufferTestData(void* rgba, int numBytes)
{
    memset(rgba, 0, numBytes);
    srand(0);
    for (int v=0; v<numBytes/4; ++v)
    {
        unsigned char r = (rand()*256/RAND_MAX);
        unsigned char g = (rand()*256/RAND_MAX);
        unsigned char b = (rand()*256/RAND_MAX);

        ((int*)rgba)[v] = (r<<0) | (g<<8) | (b<<16) | 0xff000000;
    }
}

//---------------------------------------------------------------------------------------------------
void allocateData_CUDA(int numBytes)
{
    deleteData_CUDA();

    // allocate any CUDA buffers which are shared by the benchmarks.
    //
    // (currently the benchmark functions do their own allocation of CUDA data.)
}

//---------------------------------------------------------------------------------------------------
void deleteData_CUDA()
{
    // deallocate the shared CUDA buffers...
}

//---------------------------------------------------------------------------------------------------
/// Given numBytes to work with, allocate some GL resources.
///
void allocateData_GL(int numBytes)
{
    deleteData_GL();

    // determine our pbo dimensions from the number of bytes...
    int elementBytes = sizeof(unsigned char)*4;
    g_texWidth = int(sqrtf(float(numBytes / elementBytes)));
    g_texHeight = g_texWidth;

    printf("GL allocating GL_TEXTURE_2D of (dim: %dx%d)\n", g_texWidth, g_texHeight);
    printf("GL allocating PBO of size: %.3f MB\n", float(g_texWidth*g_texHeight*elementBytes)/(1024*1024));

    // make two PBOs, (in case we decide to ever implement GL resource flipping)...
    glGenBuffers(2, g_dataGLId);
    for (int i=0; i<2; ++i)
    {
        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, g_dataGLId[i]);
		glBufferData(GL_PIXEL_UNPACK_BUFFER, numBytes, 0, GL_DYNAMIC_DRAW);
        
        int* rgba = (int*)glMapBuffer(GL_PIXEL_UNPACK_BUFFER, GL_READ_WRITE);
        assert(rgba);

        setBufferTestData(rgba, g_texWidth*g_texHeight*4);
        
        glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);

    }
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
    GL_CHECK_ERRORS();


    // create a texture for the display...
    glGenTextures(1, &g_displayTexId);
    glBindTexture(GL_TEXTURE_2D, g_displayTexId);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, image.width, image.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image.pixels);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_2D, 0);
    GL_CHECK_ERRORS();
}

//---------------------------------------------------------------------------------------------------
void deleteData_GL()
{
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    for (int i=0; i<2; ++i)
    {
        if (g_dataGLId[i])
        {
            glDeleteBuffers(1, &g_dataGLId[i]);
            g_dataGLId[i] = 0;
        }
    }

    if (g_displayTexId)
        glDeleteTextures(1, &g_displayTexId);

    g_displayTexId = 0;
    g_texWidth = 0;
    g_texHeight = 0;
}

//---------------------------------------------------------------------------------------------------
void registerResources(bool readable, bool writeable)
{
    for (int i=0; i<2; ++i)
    {
        if (!g_dataRes[i])
        {
            CUDA_CHECK(cudaGraphicsGLRegisterBuffer(&g_dataRes[i], g_dataGLId[i], cudaGraphicsRegisterFlagsNone));

            int flags = cudaGraphicsMapFlagsNone;

            if (!readable && !writeable)
                printf("WARNING: you have specified a resource that you will not read or write!\n");

            if (!readable)
            {
                flags |= cudaGraphicsMapFlagsWriteDiscard;
                //printf("CUDA:    resource registered with WRITE_DISCARD\n");
                g_useWriteDiscard = true;
            }
            else if (!writeable)
            {
                flags |= cudaGraphicsMapFlagsReadOnly;
                //printf("CUDA:    resource registered with READ_ONLY\n");
                g_useReadOnly = true;
            }
    
            CUDA_CHECK(cudaGraphicsResourceSetMapFlags(g_dataRes[i], flags));
        }
    }
}

//---------------------------------------------------------------------------------------------------
void unregisterResources()
{
    for (int i=0; i<2; ++i)
    {
        if (g_dataRes[i])
        {
	        CUDA_CHECK(cudaGraphicsUnregisterResource(g_dataRes[i]));
            g_dataRes[i] = 0;
        }
    }
}

//---------------------------------------------------------------------------------------------------
/// call the kernel.
///
void doKernel(int frame, void* buffer, cudaStream_t stream)
{
    nvtxRangePush("compute");

    // driver launches kernel asynchronously...
    doWorkInCUDA(frame, int(g_nDelayIterations_CUDA), buffer, g_copySize, stream);

    if (g_useCudaSync)
    {
        nvtxRangePush("sync-cuda");
        CUDA_CHECK(cudaStreamSynchronize(stream));
        nvtxRangePop();
    }

    nvtxRangePop();
}

//---------------------------------------------------------------------------------------------------
/// call the GL commands.
///
void doRender(int frame, GLuint id)
{
    nvtxRangePush("render");

    // GL driver launches GL commands asynchronously...
    glQueryCounter(g_glTimeStamps[frame*2+0], GL_TIMESTAMP);

    doWorkInGL(frame, int(g_nDelayIterations_GL), id);

    glQueryCounter(g_glTimeStamps[frame*2+1], GL_TIMESTAMP);

    nvtxRangePop();


    // overlay some GL...
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glColor4f(1,1,1,1);
    drawGlText(10.f, g_windowHeight-10.f, Stringf("Processing iteration %d", frame).c_str());
    drawHUD();
    glFlush();
}

//---------------------------------------------------------------------------------------------------
/// map the resource.
///
void doMap(void** ptr, cudaStream_t stream)
{
    /// INFO:
    /// cudaGraphicsMapResources behaviour.
    ///
    /// if (single-GPU)
    /// {
    ///     call is asynchronous.
    ///     upon the next synchronous call, wait for GL to finish, and do context-switch to CUDA.
    /// }
    /// else if (multi-GPU)
    /// {
    ///     if (windows)
    ///     {
    ///         if (writediscard)
    ///             do nothing.
    ///         else
    ///             stall for a time proportional to resource size: ~25%.
    ///             rest of call (i.e. the actual resource copying part) is asynchronous.
    ///             upon the next synchronous call, wait for GL to finish and copy GL resource to CUDA.
    ///     }
    ///     else if (linux)
    ///     {
    ///         if (writediscard)
    ///             do nothing.
    ///         else
    ///             call is asynchronous.
    ///             upon the next synchronous call, wait for GL to finish and copy GL resource to CUDA.
    ///     }
    /// }

    cudaGraphicsResource_t* res = &g_dataRes[0];

    nvtxRangePush("map");

    if (1)
    {
        // by waiting here, we can see how the map operation waits for GL.
        if (g_useWriteDiscard == false && g_useGLsync == true)
        {
            nvtxRangePush("sync-gl");

            // make sure we launch any cuda work before we stall this thread waiting for GL
            // or we will introduce extra delay.
            CUDA_CHECK(cudaStreamSynchronize(stream));

            if (glIsSync(g_glSyncId))
	        {
                GLenum glSyncRc;
                do { 
                    glSyncRc = glClientWaitSync(g_glSyncId, GL_SYNC_FLUSH_COMMANDS_BIT, 1000000000); 
                } while (glSyncRc == GL_TIMEOUT_EXPIRED);
            }
            nvtxRangePop();
        }
    }

    // This call is always asynchronous.
    CUDA_CHECK(cudaGraphicsMapResources(1, res, stream));

    size_t size = 0;
    CUDA_CHECK(cudaGraphicsResourceGetMappedPointer(ptr, &size, *res));
    assert(size == g_copySize);

    // If we are using synchronous execution mode, then optionally synchronize the 
    // CUDA stream so that we can see the actual duration for the map in the profiler.

    if (g_useCudaSync)
    {
        // now wait for the map operation to actually do something.
        nvtxRangePush("sync-cuda");
        CUDA_CHECK(cudaStreamSynchronize(stream)); 
        nvtxRangePop();
    }

    nvtxRangePop();
}

//---------------------------------------------------------------------------------------------------
/// unmap the resource.
///
void doUnmap(cudaStream_t stream)
{
    /// INFO:
    /// cudaGraphicsUnmapResources behaviour.
    ///
    /// if (single-GPU)
    /// {
    ///     if (streamwait commands are in the stream)
    ///         synchronize stream upto the event.
    ///
    ///     call does nothing (because the preceeding map call already required gl to finish upon any synchronization).
    /// }
    /// else if (multi-GPU)
    /// {
    ///     if (windows)
    ///     {
    ///         synchronize CUDA, and wait for GL to finish. <-- (WDDM! Does it do this in TCC mode?)
    ///
    ///         if (readonly)
    ///             do nothing. 
    ///         else
    ///             copy CUDA resource to GL, (and has some overhead proportional to resource size: ~50%).
    ///     }
    ///     else if (linux)
    ///     {
    ///         call is asynchronous.
    ///
    ///         if (readonly)
    ///             do nothing. 
    ///         else
    ///             upon the next synchronous call, wait for GL to finish and copy CUDA resource to GL.
    ///     }
    /// }

    cudaGraphicsResource_t* res = &g_dataRes[0];

    nvtxRangePush("unmap");

    if (1)
    {
        // by waiting here, we can see how the unmap operation waits for GL in the profiler.
        if (g_useReadOnly == false && g_useGLsync == true)
        {
            nvtxRangePush("sync-gl");

            /// CAVEAT: use-glsync with the "manual" method will slow it down because
            /// it won't be able to overlap render with the copy.
            /// (This is only used to show the GL wait in the profiler for debugging.)

            // make sure we launch any cuda work before we stall this thread waiting for GL
            // or we will introduce extra delay.
            CUDA_CHECK(cudaStreamSynchronize(stream));

            if (glIsSync(g_glSyncId))
	        {
                GLenum glSyncRc;
                do { 
                    glSyncRc = glClientWaitSync(g_glSyncId, GL_SYNC_FLUSH_COMMANDS_BIT, 1000000000); 
                } while (glSyncRc == GL_TIMEOUT_EXPIRED);
            }
            nvtxRangePop();
        }
    }

    CUDA_CHECK(cudaGraphicsUnmapResources(1, res, stream));

    // We synchronize the CUDA stream so that we can get the actual duration for the unmap...
    /// NOTE:
    ///
    /// on windows: "cudaGraphicsUnmapResources" is synchronous when multi-gpu, 
	/// or it does nothing when single-gpu. so there's no need for this sync.
    ///
    /// on linux: it is asynchronous, so we actually benefit from this sync. :-)
    
    if (g_useCudaSync)
    {
        nvtxRangePush("sync-cuda");
        CUDA_CHECK(cudaStreamSynchronize(stream));
        nvtxRangePop();
    }

    nvtxRangePop();
}

//---------------------------------------------------------------------------------------------------
/// dump the GL timing information that gets collected during benchmarks.
///
void printGlTiming()
{
    if (g_printGlTiming)
    {
        GLuint64EXT timeStampStart = 0;
        GLuint64EXT timeStamp[2];
	    glGetQueryObjectui64vEXT(g_glTimeStampStart, GL_QUERY_RESULT, &timeStampStart);
	    GL_CHECK_ERRORS(); 
	    float timeStampStartf = timeStampStart*0.000001f;	

        printf("\n");
        for (int i=0; i<g_numIterations; ++i)
        {
	        glGetQueryObjectui64vEXT(g_glTimeStamps[i*2+0], GL_QUERY_RESULT, &timeStamp[0]);
    	    glGetQueryObjectui64vEXT(g_glTimeStamps[i*2+1], GL_QUERY_RESULT, &timeStamp[1]);
	        GL_CHECK_ERRORS(); 
    	    float t1 = timeStamp[0]*0.000001f - timeStampStartf;
    	    float t2 = timeStamp[1]*0.000001f - timeStampStartf;

            printf("\t%d. gl(%f - %f)\n",i,t1,t2);
        }
    }
}

//---------------------------------------------------------------------------------------------------
/// set the data and work sizes.
///
void setWorkConfig(float cudaMs, float glMs, float copySize, float copyMs, bool forceTuning)
{
    startGlContext(&glContext);

    if (glMs < 5)
        glMs = 5;
    
    if (cudaMs < 5)
        cudaMs = 5;

    if (copySize < 5*1024*1024)
        copySize = 5*1024*1024;

    if (copyMs > 0 && copyMs < 5)
        copyMs = 5;

    assert(copySize > 0 || copyMs > 0);

    // these benchmarks are now invalid, so clear them.
    g_prevElapsedTimes.clear();
    g_prevMethodNames.clear();

    printf("------------------------------------------------------------------------\n");

    float tuningMs = 0;
    bool dataSizeAdjusted = false;

    if (forceTuning || copySize != g_copySize)
    {
        deleteData_CUDA();
        deleteData_GL();
    
        if (copyMs >= 0)
        {
            g_nDelayIterations_Copy = 65536;
            printf("Tuning copy to take ~%1.2f ms... ", copyMs);
            tuningMs = tuneLinearKnob(&g_nDelayIterations_Copy, benchmark_Copy, g_numIterations, copyMs, 9);
            g_copySize = int(g_nDelayIterations_Copy);
            printf("Resource using %.1f MB to take %1.2f ms.\n", float(g_copySize)/(1024*1024), tuningMs);
            g_actualMs_Copy = tuningMs;
        }
        else
        {
            g_copySize = int(copySize);
            g_nDelayIterations_Copy = copySize;
            printf("Resource using %.1f MB.\n", float(g_copySize)/(1024*1024));
            // now we have to work out how long it takes.
            g_actualMs_Copy = benchmark_Copy(g_numIterations);
        }

        // reallocate the resource.
        allocateData_CUDA(g_copySize);
        allocateData_GL(g_copySize);

        //dataSizeAdjusted = true;
    }

    if (forceTuning || glMs != g_targetMsecGL || dataSizeAdjusted == true)
    {
        g_nDelayIterations_GL = 65536;
        printf("Tuning GL to take ~%1.2f ms... ", glMs);
        tuningMs = tuneLinearKnob(&g_nDelayIterations_GL, benchmark_GL, g_numIterations, glMs, 5);
        printf("GL using %d iterations to take %1.2f ms.\n", (int)g_nDelayIterations_GL, tuningMs);
        g_targetMsecGL = glMs;
        g_actualMs_GL = tuningMs;
    }

    if (forceTuning || cudaMs != g_targetMsecCUDA || dataSizeAdjusted == true)
    {
        g_nDelayIterations_CUDA = 65536;
        printf("Tuning CUDA to take ~%1.2f ms... ", cudaMs);
        tuningMs = tuneLinearKnob(&g_nDelayIterations_CUDA, benchmark_CUDA, g_numIterations, cudaMs, 5);
        printf("CUDA using %d iterations to take %1.2f ms.\n", (int)g_nDelayIterations_CUDA, tuningMs);
        g_targetMsecCUDA = cudaMs;
        g_actualMs_CUDA = tuningMs;
    }

    printf("\n");

    initializedWorkConfig = true;

    startGlContext(&windowContext);
    glClear(GL_COLOR_BUFFER_BIT);
    drawLogo(g_numIterations-1);
    drawHUD();
    glutSwapBuffers();
}

//---------------------------------------------------------------------------------------------------
/// Set the device configuration.
///
void setDeviceConfig(int cudaDevice)
{
    // before switching device, cleanup any context-based resources.
    deleteData_CUDA();
    deleteData_GL();

    // these benchmarks are now invalid, so clear them.
    g_prevElapsedTimes.clear();
    g_prevMethodNames.clear();

    g_actualMs_CUDA = -1;
    g_actualMs_GL = -1;
    g_actualMs_Copy = -1;

    CUDA_CHECK(cudaDeviceReset());

	g_cudaDeviceIndex = cudaDevice;
	
    cudaDeviceProp prop;
    CUDA_CHECK(cudaGetDeviceProperties(&prop, cudaDevice));

    g_cudaDeviceName = std::string(prop.name);

    printf("\n");
    printf("------------------------------------------------------------------------\n");
    printf("CUDA:    using device %1d %21s, pci=%04d:%02d:%02d, engines=%d \n", cudaDevice, prop.name, prop.pciDomainID, prop.pciBusID, prop.pciDeviceID, prop.asyncEngineCount);
    printf("\n");

	bool multiGPU = false;
	for (unsigned int i = 0; i < g_glDeviceCount; i++)
	{
		if (g_glDeviceIndices[i] == cudaDevice)
		{
			multiGPU = false;
			printf("         * CUDA and OpenGL contexts share the same device.\n");
		}
		else
		{
			multiGPU = true;
			printf("         * CUDA and OpenGL contexts reside on different devices.\n");
		}
	}

	if (multiGPU)
    { 
		//P2P is unidirectional have to set it for both cards
        int canAccessPeer = 0;
		//1. cudaDevice to access GLdevice
		CUDA_CHECK(cudaSetDevice(cudaDevice));
        cudaDeviceCanAccessPeer(&canAccessPeer, cudaDevice, g_glDeviceIndex);
		if (canAccessPeer) {
			CUDA_CHECK(cudaDeviceEnablePeerAccess( g_glDeviceIndex, 0 ));
		   printf("         * CUDA device can access GL device\n");
		}
		else
		   printf("         * CUDA device CANNOT P2P access GL device\n");

		//2. GL Device to access cudadevice
		CUDA_CHECK(cudaSetDevice(g_glDeviceIndex));
        cudaDeviceCanAccessPeer(&canAccessPeer, g_glDeviceIndex, cudaDevice);
		if (canAccessPeer) {
			CUDA_CHECK(cudaDeviceEnablePeerAccess( cudaDevice, 0 ));
		   printf("         * GL device can access CUDA device\n");
		}
		else
		   printf("         * GL device CANNOT P2P access CUDA device\n");

    }
    printf("------------------------------------------------------------------------\n");
    printf("\n");

    // initialize the CUDA device
    CUDA_CHECK(cudaSetDevice(g_cudaDeviceIndex));
    CUDA_CHECK(cudaFree(0)); 

    setWorkConfig(g_targetMsecCUDA, g_targetMsecGL, float(g_copySize), g_targetMsecCopy, true);
}

//---------------------------------------------------------------------------------------------------
/// run all the benchmarks non-interactively.
///
void batchRunBenchmarks(int cudaDevice)
{
    setDeviceConfig(cudaDevice);

    cudaProfilerStart(); 

    //-----------------------------------------------
    memset((void*)&g_eventAttrib, 0, sizeof(nvtxEventAttributes_t));
    g_eventAttrib.version = NVTX_VERSION;
    g_eventAttrib.size = NVTX_EVENT_ATTRIB_STRUCT_SIZE;
    g_eventAttrib.colorType = NVTX_COLOR_ARGB;
    g_eventAttrib.messageType = NVTX_MESSAGE_TYPE_ASCII;

    // make a different colour in the profiler for each config...
    switch (cudaDevice)
    {
    case 0:
        g_eventAttrib.color = 0xFF0088FF;
        g_eventAttrib.message.ascii = "device 0";
        break;
    case 1:
        g_eventAttrib.color = 0xFFFF8800;
        g_eventAttrib.message.ascii = "device 1";
        break;
    case 2:
        g_eventAttrib.color = 0xFF0F880F;
        g_eventAttrib.message.ascii = "device 2";
        break;
    case 3:
	default:
        g_eventAttrib.color = 0xFFFFFF88;
        g_eventAttrib.message.ascii = "device 3";
        break;
    }

    nvtxRangeId_t id1;
    id1 = nvtxRangeStartEx(&g_eventAttrib);

    runAllBenchmarks(g_numIterations);

    nvtxRangeEnd(id1);

    cudaProfilerStop(); 
}

//---------------------------------------------------------------------------------------------------
void printHotkeys()
{
    printf("\n");
	printf("MultiGPU CUDA/OpenGL Interoperability Benchmark\n");
    printf("\n");
    printf("Hotkeys:\n");
    printf("\n");
	printf("\th                  : Print this help message\n");
	printf("\tq                  : Quit\n");
	printf("\tr                  : Run all benchmark tests\n");
	printf("\t1 - 9              : Run specific benchmark test. (Not all numbers are valid - check source code)\n");
	printf("\td                  : Cycle through device configurations\n");
	printf("\t[  /  ]            : Decrement / Increment CUDA work\n");
	printf("\t-  /  =            : Decrement / Increment GL work\n");
	printf("\t;  /  \'            : Decrement / Increment data size\n");
    printf("\n");
}

//---------------------------------------------------------------------------------------------------
void printHelp()
{
    printf("\n");
	printf("MultiGPU CUDA/OpenGL Interoperability Benchmark\n\n");
	printf("Program parameters:\n");
    printf("\n");
    printf("\n");
	printf("\t-work-cuda <#>     : The time to spend doing CUDA work in msec. (default=10.0)\n");
	printf("\t-work-gl <#>       : The time to spend doing OpenGL work in msec. (default=10.0)\n");
    printf("\n");
	printf("\t-work-size <#>     : The number of megabytes of data. (This is only used if work-copy is not specified) (default=10.0 MB)\n");
    printf("\t-work-copy <#>     : The time to spend doing data copy work in msec. (default=not-used)\n");
    printf("\n");
    printf("\t-use-streams <#>   : stream mode: (0=synchronous, 1=default-stream, 2=new stream, 3=quadro&tesla-stream, 4=quadro-stream only). (default=0)\n");
    printf("\t-no-multigpu       : do not do multi-gpu benchmarking. (default=false)\n");
    printf("\t-use-cudasync      : synchronize CUDA so we can see the wait in the profiler. (default=false)\n");
    printf("\t-use-glsync        : synchronize GL so we can see the wait in the profiler. (default=false)\n");
	printf("\t-iterations <#>    : The number of iterations to average the timing data over (default=20).\n");
    printf("\t-print-gltimers    : print GL timing information. (default=false)\n");
    printf("\n");
    printf("\t-prompt            : Prompt for keypress before termination. (useful when using debugger). (default=false)\n");
	printf("\t-batch             : Run in non-interactive mode (default=false).\n");
	printf("\t-hide              : Hide the window and just get the text output (default=false).\n");

    
    printf("\n");
    printf("Examples:\n");
    printf("\n");
    printf("Simple benchmark:\n");
    printf("interop -work-cuda 10 -work-gl 10 -work-copy 5 \n");
    printf("\n");
    printf("benchmark showing wait for opengl in profiler:\n");
    printf("interop -work-cuda 20 -work-gl 5 -work-copy 10 -use-glsync \n");
    printf("\n");
    printf("benchmark using specified data size of 3.1 MB, running in non-interactive mode: \n");
    printf("interop -work-cuda 20 -work-gl 5 -work-size 3.1 -batch \n");
    printf("\n");
}

//---------------------------------------------------------------------------------------------------
/// Parse the arguments.
///
int parseArgs(int count, char **argv)
{	
	// Parse command line arguments
	for(int i = 0; i < count;)
	{
		const char *szBuffer = argv[i++];
		
		if (!strcasecmp("-help", szBuffer)) 
        {
            printHelp();
			return 0;
		}

		if (!strcasecmp("-prompt", szBuffer)) 
        {
			g_waitForKey = true;
		}
        else if(!strcasecmp("-work-cuda", szBuffer)) 
        {
			if(i == count)
				return -1;
			szBuffer = argv[i++];	
			g_targetMsecCUDA = float(atof(szBuffer));			
		}
        else if(!strcasecmp("-work-gl", szBuffer)) 
        {
			if(i == count)
				return -1;
			szBuffer = argv[i++];	
			g_targetMsecGL = float(atof(szBuffer));			
		}
        else if(!strcasecmp("-work-copy", szBuffer)) 
        {
			if(i == count)
				return -1;
			szBuffer = argv[i++];	
			g_targetMsecCopy = float(atof(szBuffer));
            g_copySize = -1; // we will determine the copySize value from the
		}
        else if(!strcasecmp("-work-size", szBuffer)) 
        {
			if(i == count)
				return -1;
			szBuffer = argv[i++];	
			g_copySize = int(atof(szBuffer)*1024*1024);
            g_targetMsecCopy = -1;
		}
        else if(!strcasecmp("-iterations", szBuffer)) 
        {
			if(i == count)
				return -1;
			szBuffer = argv[i++];	
			g_numIterations = atoi(szBuffer);			
		}
        else if(!strcasecmp("-use-cudasync", szBuffer)) 
        {
			g_useCudaSync = true;
		}
        else if(!strcasecmp("-use-glsync", szBuffer)) 
        {
            g_useGLsync = true;
		}
        else if(!strcasecmp("-batch", szBuffer)) 
        {
            g_useBatchMode = true;
		}
        else if(!strcasecmp("-hide", szBuffer)) 
        {
            g_hideWindow = true;
		}
        else if(!strcasecmp("-print-gltimers", szBuffer)) 
        {
            g_printGlTiming = true;
		}
		else
        {
			return -1;
        }
	}

	return 1;
}

//---------------------------------------------------------------------------------------------------
void initializeGL()
{
    //printf("INFO:    initializing...\n");

    if (!glewInit() == GLEW_OK)
    {
        printf("ERROR:   Failed to initialize GLEW.\n");
        exit(1);
    }

#if 0
    {
        const int selectedGpu = 0;

        const int MAX_GPUS = 4;
        HGPUNV gpuHandles[MAX_GPUS];
        int numGpus = 0;
        while (numGpus < MAX_GPUS)
        {
            if (wglEnumGpusNV(numGpus, &gpuHandles[numGpus]) == false)
                break;
            numGpus++;
        }
        STDERR(numGpus);

        HGPUNV gpuMask[MAX_GPUS];
        gpuMask[0]=gpuHandles[selectedGpu];
        gpuMask[1]=0;

        HGLRC affinityGLRC = 0;
        HDC affinityDC = 0;
        HDC affinityDCtmp = wglCreateAffinityDCNV(gpuMask);
        /*
        // Set a pixelformat on the affinity-DC...
        PIXELFORMATDESCRIPTOR pfd;
        int pf = ChoosePixelFormat(affinityDCtmp, &pfd);
        SetPixelFormat(affinityDC, pf, &pfd);
        DescribePixelFormat(affinityDCtmp, pf, sizeof(PIXELFORMATDESCRIPTOR), &pfd);
        affinityDC = affinityDCtmp;
        */

        // Setup desired pixelformat attributes for the pbuffer
        // including WGL_DRAW_TO_PBUFFER_ARB.
        
        int          width = 256, height = 256, format = 0;
        unsigned int nformats;

        int attribList[] =
        {
            WGL_RED_BITS_ARB,               8,
            WGL_GREEN_BITS_ARB,             8,
            WGL_BLUE_BITS_ARB,              8,
            WGL_ALPHA_BITS_ARB,             8,
            WGL_STENCIL_BITS_ARB,           0,
            WGL_DEPTH_BITS_ARB,             0,
            WGL_DRAW_TO_PBUFFER_ARB,        true,
            0,
        };

        wglChoosePixelFormatARB(affinityDCtmp, attribList, NULL, 1, &format, &nformats);
        pbufferHandle = wglCreatePbufferARB(affinityDCtmp, format, width, height, NULL);

        // pbufferAffDC will have the same affinity-mask as affDC.
        affinityDC = wglGetPbufferDCARB(pbufferHandle);

        // now create the GL context...
        affinityGLRC = wglCreateContext(affinityDC);
        wglMakeCurrent(affinityDC, affinityGLRC);

        glContext = getGlCurrentContextData();

        startGlContext(&glContext);

        glViewport(0, 0, width, height);
        glMatrixMode(GL_PROJECTION);
	    glLoadIdentity();
	    glOrtho(0, width, height-1, 0, -1, 1);
	    glMatrixMode(GL_MODELVIEW);
	    glLoadIdentity();
    }
#else
    glContext = windowContext;
#endif

    // Print all devices that are spanned by the current GL context...
    // CAVEAT: we assume a maximum of 16 glDevices.
    g_glDeviceCount = 0;
    CUDA_CHECK(cudaGLGetDevices(&g_glDeviceCount, g_glDeviceIndices, 16, cudaGLDeviceListAll));
    printf("CUDA:    OpenGL is using CUDA device(s): ");
    for (unsigned int i=0; i<g_glDeviceCount; ++i)
        printf("%s%d", (i==0) ? "" : ", ", g_glDeviceIndices[i]);
    printf("\n");

    // we'll just use the first glDevice.
    g_glDeviceIndex = g_glDeviceIndices[0];

    cudaDeviceProp prop;
    CUDA_CHECK(cudaGetDeviceProperties(&prop, g_glDeviceIndex));
    g_glDeviceName = std::string(prop.name);
    //const GLubyte* renderer = glGetString(GL_RENDERER);

    printf("\n");
    printf("------------------------------------------------------------------------\n");
    //printf("GL:      renderer=\"%24s\"\n", renderer);
    printf("GL:      using device %1d %21s, pci=%04d:%02d:%02d, engines=%d \n", g_glDeviceIndex, prop.name, prop.pciDomainID, prop.pciBusID, prop.pciDeviceID, prop.asyncEngineCount);
    printf("------------------------------------------------------------------------\n");


    //cudaProfilerInitialize(...); 

    if (isFile("logo.png"))
    {
        image.pixels = stbi_load("logo.png", &image.width, &image.height, &image.numComponents, 4);
        assert(image.numComponents == 4);
        assert(image.pixels != 0);
    }
    else
    {
        printf("WARNING: Can't find logo.png\n");
    }

    // Create a shader which is super-inefficient, which we'll use to simulate a complex draw call.
    initGL();

    glGenQueries(1, &g_glTimeStampStart);
    g_glTimeStamps = new GLuint[g_numIterations*2];
    glGenQueries(g_numIterations*2, g_glTimeStamps);

    g_cudaDeviceCount = 0;
    cudaGetDeviceCount(&g_cudaDeviceCount);
    CUDA_CHECK_ERRORS();
}

//---------------------------------------------------------------------------------------------------
void cleanup()
{
    //printf("INFO:    cleaning up.\n");
    // we can't do GL_CHECK_ERRORS because we are inside the atexit function!

    // before switching device, cleanup.
    deleteData_CUDA();
    deleteData_GL();

    cleanupGL();

    // this is essential for the profiling information to be flushed.
    CUDA_CHECK(cudaDeviceReset());

    glDeleteBuffers(2, g_dataGLId);
    //GL_CHECK_ERRORS();

    glDeleteQueries(1, &g_glTimeStampStart);
    glDeleteQueries(g_numIterations*2, g_glTimeStamps);
    delete[] g_glTimeStamps;
}

//---------------------------------------------------------------------------------------------------
/// entry point.
///
int main(int argc, char *argv[])
{
    int ret = parseArgs(argc-1, &argv[1]);
	if (ret < 0)
	{
		printf("Error - Incorrect parameters.\n");
        printHelp();
		return -1;
	}
	else if (ret == 0)
	{
		return 0;
	}

    // initialize OpenGL and GL-window...
    int window;
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA);
    glutInitWindowSize(g_windowWidth, g_windowHeight);
    glutInitWindowPosition(0, 0);
    window = glutCreateWindow("Interop-benchmark");
    windowContext = getGlCurrentContextData();

    // CAVEAT: hiding the window causes Linux to optimize GL away!
#ifdef WIN32
	if (g_hideWindow && g_useBatchMode == true)
        glutHideWindow();
#endif

    initializeGL();
    onReshape(g_windowWidth, g_windowHeight);

    if (g_useBatchMode == true)
    {
        // We don't display an interactive GUI.
        // Instead, we will run the benchmarks and print the results...

        int nConfigurations = g_cudaDeviceCount;

        for (int i=0; i<nConfigurations; ++i)
        {
            if (i == 0)
            {
                // use the same compute device as the GL context.
                g_cudaDeviceIndex = g_glDeviceIndex;
            }
            else
            {
                // use a different compute device from the GL context.
                g_cudaDeviceIndex = (g_glDeviceIndex + i)%g_cudaDeviceCount;
            }

            // run the benchmark test with this configuration of devices...
            batchRunBenchmarks(g_cudaDeviceIndex);

            if (i < nConfigurations-1)
            {
                // prompt inbetween configurations.
                if (g_waitForKey == true && g_useBatchMode == false)
                    promptForKey();
            }
        }

        if (g_waitForKey || g_hideWindow == false)
            promptForKey();

        cleanup();
        glutDestroyWindow(window);
    }
    else
    {
        glutSetWindow(window);
        glutKeyboardFunc(onKeyboard);
        glutReshapeFunc(onReshape);
        glutDisplayFunc(onDisplay);
        atexit(cleanup);

        setDeviceConfig(g_cudaDeviceIndex);

        printHotkeys();

        glutMainLoop();
    }

    return 0;
}

//---------------------------------------------------------------------------------------------------
