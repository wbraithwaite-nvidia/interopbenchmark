/* ---------------------------------------------------------------------------
 * This software is in the public domain, furnished "as is", without technical
 * support, and with no warranty, express or implied, as to its usefulness for
 * any purpose.

 * Author: Wil Braithwaite.
 *
 */


#include "gl_utils.h"
#include "std_utils.h"

#include <GL/freeglut.h>

namespace Easy
{

//---------------------------------------------------------------------------------------------------
void drawGlText(GLfloat x, GLfloat y, const char *s, void *font)
{
    if (!font)
        font = GLUT_BITMAP_9_BY_15;
    glRasterPos2f(x, y);
    int len = (int)strlen(s);
    for (int i = 0; i < len; i++)
        glutBitmapCharacter(font, s[i]);
}

//---------------------------------------------------------------------------------------------------
void drawGlRectangle(const float lx, const float ly, const float hx, const float hy)
{
    const float p[4*2]=
    {
        lx,ly,
        hx,ly,
        hx,hy,
        lx,hy,
    };
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(2,GL_FLOAT,0,p);
    glDrawArrays(GL_TRIANGLE_FAN,0,4);
    glDisableClientState(GL_VERTEX_ARRAY);
}

//------------------------------------------------------------------------------------------
bool __glCheckErrors(const char *file, const int line)
{
    bool first=true;

    for (;;)
    {
        int e=glGetError();

        if (e==GL_NO_ERROR)
        {
            if (!first)
                std::cerr << std::endl;
            return first;
        }

        first=false;

        static char s[1024];

        sprintf(s,"file <%s>, line %i:",file,line);

        if (e==GL_INVALID_ENUM)
            std::cerr<<"GL Error: "<<s<<" GL_INVALID_ENUM" << ", ";
        if (e==GL_INVALID_VALUE)
            std::cerr<<"GL Error: "<<s<<" GL_INVALID_VALUE" << ", ";
        if (e==GL_INVALID_OPERATION)
            std::cerr<<"GL Error: "<<s<<" GL_INVALID_OPERATION" << ", ";
        if (e==GL_STACK_OVERFLOW)
            std::cerr<<"GL Error: "<<s<<" GL_STACK_OVERFLOW" << ", ";
        if (e==GL_STACK_UNDERFLOW)
            std::cerr<<"GL Error: "<<s<<" GL_STACK_UNDERFLOW" << ", ";
        if (e==GL_OUT_OF_MEMORY)
            std::cerr<<"GL Error: "<<s<<" GL_OUT_OF_MEMORY" << ", ";
        else
            std::cerr << "GL Error: " << s << " "  << glewGetErrorString(e) << ", ";
    }
}

//---------------------------------------------------------------------------------------
#if defined(_WIN32)

GlContextData getGlCurrentContextData()
{
    GlContextData d;
    d.hdc = wglGetCurrentDC();
	d.hglrc = wglGetCurrentContext();
    return d;
}

//------------------------------------------------------------------------------------------
void* getGlCurrentContext()
{
    return (void*)wglGetCurrentContext();
}

//------------------------------------------------------------------------------------------
void* getGlCurrentDisplay()
{
	return (void*)wglGetCurrentDC();
}

//---------------------------------------------------------------------------------------
bool createGlContext(GlContextData* data, bool share)
{
	HGLRC hglrc = wglCreateContext (data->hdc);
	if (hglrc == 0)
	{
		DWORD err = GetLastError();
		std::cerr << Stringf("Failed on wglCreateContext: ") << err << std::endl;
        return false;
	}

    // if there is already a context in the data, then share it.
	if (share)
	{
        if (!data->hglrc)
        {
            std::cerr << Stringf("The context to share is not valid!") << std::endl;
            return false;
        }

        bool rc = (wglShareLists(data->hglrc, hglrc) == TRUE);
        if (rc == false)
        {
		    DWORD err = GetLastError();
		    std::cerr << Stringf("Failed on wglShareLists: ") << err << std::endl;
            return false;
        }
	}
	/*
    // set to the old context!
    wglMakeCurrent(data->hdc, data->hglrc);
    if(glewInit() != GLEW_OK)
    {
        std::cerr << Stringf("Failed to initialize GLEW") << std::endl;
        return false;
    }
    */
    data->hglrc = hglrc;
	//std::cout << Stringf("Created GL context(%p) for display(%p) ", data->hglrc, data->hdc) << std::endl;
	return true;
}

//---------------------------------------------------------------------------------------
void destroyGlContext(GlContextData* data)
{
	if (!data->hdc)
		return;

    assert(data->hglrc);
	//std::cout << Stringf("Destroying GL context(%p) for display(%p)", data->hglrc, data->hdc) << std::endl;
    wglDeleteContext(data->hglrc);

	data->hdc = 0;
	data->hglrc= 0;
}

//---------------------------------------------------------------------------------------
void startGlContext(GlContextData* data)
{
	if(!data->hdc)
		return;

	assert(data->hglrc);
    wglMakeCurrent(data->hdc, data->hglrc);
}

//---------------------------------------------------------------------------------------
void endGlContext(GlContextData* data)
{
	if(!data->hdc)
		return;

    wglMakeCurrent(data->hdc, 0);
}

//---------------------------------------------------------------------------------------
#else

GlContextData getGlCurrentContextData()
{
    GlContextData d;
    d.xdisplay = glXGetCurrentDisplay();
	d.glxcontext = glXGetCurrentContext();
    int xscreen = DefaultScreen(d.xdisplay);
    d.xwindow = glXGetCurrentDrawable();//RootWindow(d.xdisplay, xscreen);
    return d;
}

//---------------------------------------------------------------------------------------
void* getGlCurrentContext()
{
    return (void*)glXGetCurrentContext();
}

//---------------------------------------------------------------------------------------
void* getGlCurrentDisplay()
{
    return (void*)glXGetCurrentDisplay();
}

//---------------------------------------------------------------------------------------
bool createGlContext(GlContextData* data, bool share)
{
    Display* xdisplay = data->xdisplay;
    char* displayName = DisplayString(xdisplay);
    int xscreen = DefaultScreen(xdisplay);
    Window xwindow = RootWindow(xdisplay, xscreen);

    GLint attributes[] = {GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, None};
    XVisualInfo *xvisual = glXChooseVisual(xdisplay, xscreen, attributes);
    if(xvisual == NULL)
    {
        std::cerr << Stringf("Failed to choose GLX-visual") << std::endl;
        return false;
    }
    XSync(xdisplay,false);
    GLXContext glxcontext = glXCreateContext(xdisplay, xvisual, (share)?data->glxcontext:0, true);
    if(glxcontext == NULL)
    {
        std::cerr << Stringf("Failed to initialize GL-context for ") << displayName << std::endl;
        return false;
    }
    XSync(xdisplay,false);
    if(!glXMakeCurrent(xdisplay, xwindow, glxcontext))
    {
        std::cerr << Stringf("Failed to make GL-context current for ") << displayName << std::endl;
        return false;
    }
    XSync(xdisplay,false);
    //std::cout << Stringf("Created GL-context for ") << displayName << std::endl;
    /*if(glewInit() != GLEW_OK)
    {
        std::cerr << Stringf("Failed to initialize GLEW") << std::endl;
        return false;
    }*/

	data->glxcontext = glxcontext;
	data->xwindow = xwindow;

	//std::cout << Stringf("Creating GL context(%p) for display(%p) ", glxcontext, xdisplay) << std::endl;
	return true;
}

//---------------------------------------------------------------------------------------
void destroyGlContext(GlContextData* data)
{
	if (!data->xdisplay)
		return;

    assert(data->glxcontext);
	//std::cout << Stringf("Destroying GLX display(%p) and context(%p)", data->xdisplay, data->glxcontext) << std::endl;
    glXDestroyContext( data->xdisplay, data->glxcontext);
}

//---------------------------------------------------------------------------------------
void startGlContext(GlContextData* data)
{
	if (!data->xdisplay)
		return;

	assert(data->glxcontext);
	assert(data->xwindow);
    glXMakeCurrent(data->xdisplay, data->xwindow, data->glxcontext);
}

//---------------------------------------------------------------------------------------
void endGlContext(GlContextData* data)
{
	if(!data->xdisplay)
		return;

    glXMakeCurrent(data->xdisplay, None, 0);
}

#endif
}
