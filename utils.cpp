/*
 * Copyright 1993-2012 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

#include "utils.h"
#include <cmath>

float tuneLinearKnob(float* inputValuePtr, float (*func)(int), int numIterations, float targetValue, int maxSteps, int verbosity)
{
    nvtxEventAttributes_t eventAttrib = {0};
    eventAttrib.version = NVTX_VERSION;
    eventAttrib.size = NVTX_EVENT_ATTRIB_STRUCT_SIZE;
    eventAttrib.colorType = NVTX_COLOR_ARGB;
    eventAttrib.messageType = NVTX_MESSAGE_TYPE_ASCII;
    eventAttrib.color = 0xFF888888;
    eventAttrib.message.ascii = "Tuning";
    nvtxRangeId_t initNvtx = nvtxRangeStartEx(&eventAttrib);
	
    int step = 0;
	float evalValue;
    while (1)
    {
		//if (verbosity)
		//	printf("knob = %d\n", *knob);
        float knobValue = *inputValuePtr;

        // the func must use the pointer to evaluate its returned result: (inputValuePtr)
        evalValue = func(numIterations);

        float f = evalValue / targetValue;
        float relerr = 1-f;

        if (verbosity)
            printf("\n%2d. func(%f) = %1.2f     (factor = %f, error = %f)", step, knobValue, evalValue, f, relerr);

        //if (fabs(relerr) < 0.05)
            //break;

        *inputValuePtr = (int)(knobValue / f);
        if (*inputValuePtr <= 0) 
            *inputValuePtr = 1;

        if (++step == maxSteps) 
            break;

        //if (fabs(relerr) < 0.05)
        //    maxSteps = step+1;
    }

    if (verbosity)
        printf("\n");

    nvtxRangeEnd(initNvtx);

	return evalValue;
}

