/*
 * Copyright 1993-2012 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

#include "interop.h"

using namespace Easy;

//---------------------------------------------------------------------------------------------------
/// Benchmark CUDA and GL, with data shared through interop
/// but this is done manually by having a cuda context on the display gpu.
///
/// We must use CPU based timing here since there is a mix of APIs with interdependant calls that
/// are possibly not CPU asynchronous.
///
float benchmark_Manual_WriteDiscard_Async_P2P(int nTimingIterations)
{
    //-------------------------------------------------------------------
    CUDA_CHECK(cudaSetDevice(g_cudaDeviceIndex));

    cudaStream_t stream = 0; //stream used by cuda
    CUDA_CHECK(cudaStreamCreate(&stream));

    cudaEvent_t kernelEventComplete = 0; //belongs to cudaDevice
    cudaEventCreate(&kernelEventComplete);

    //-------------------------------------------------------------------
    // create the references on the display GPU...
    CUDA_CHECK(cudaSetDevice(g_glDeviceIndex));
	CUDA_CHECK(cudaFree(0));
    registerResources(false, true);

    // NB. we COULD set all streams to be 0, everything happens synchronously, but let's not.
    cudaStream_t copyStreamCudaToGl = 0; //for write discard, copy from cudaDevice->glDevice 
    //cudaStream_t copyStreamGlToCuda = 0; //for read-only, copy from gldevice->cudadevice
    CUDA_CHECK(cudaStreamCreate(&copyStreamCudaToGl));
    //CUDA_CHECK(cudaStreamCreate(&copyStreamGlToCuda));

    //cudaEvent_t copyEventGlToCudaComplete = 0; //belongs to glDevice, after it copies gl data(mapped) -> cuda (readonly or bidi)
	//CUDA_CHECK(cudaEventCreate(&copyEventGlToCudaComplete));

    //-------------------------------------------------------------------
    // allocate the memory...
    CUDA_CHECK(cudaSetDevice(g_cudaDeviceIndex));

    void* d_buffer = 0;
    CUDA_CHECK(cudaMalloc(&d_buffer, g_copySize));
    
    //-------------------------------------------------------------------
    // glFinish is necessary call to make when doing CPU based timings to ensure 
    // that all the preceding OpenGL commands are done before we start timing.
	glFinish();

    // cudaStreamSynchronize is necessary call to make when doing CPU based timings to 
    // ensure that all the preceding CUDA commands are done before we start timing.
	CUDA_CHECK(cudaDeviceSynchronize());
	
    nvtxRangePush("manual-p2p");
    Timer timer;

    glQueryCounter(g_glTimeStampStart, GL_TIMESTAMP);
    glFlush();

    //-------------------------------------------------------------------
    void* d_mappedPtr;

    for (int iter=0; iter<nTimingIterations; ++iter) 
    {
        doKernel(iter, d_buffer, stream);
		CUDA_CHECK(cudaEventRecord(kernelEventComplete, stream));
		
        //-------------------------------------------------------------------
        // now use the cuda context on the display GPU...

        CUDA_CHECK(cudaSetDevice(g_glDeviceIndex));
		
        doMap((void**)&d_mappedPtr, 0);

		// copy from the compute-device's buffer to the resource's mapped device-memory.
		// but after the kernel on compute-device is complete.
		CUDA_CHECK(cudaStreamWaitEvent(copyStreamCudaToGl, kernelEventComplete, 0));

		// p2p copy from cuda device -> gl device
		CUDA_CHECK(cudaMemcpyPeerAsync(d_mappedPtr, g_glDeviceIndex, d_buffer, g_cudaDeviceIndex, g_copySize,  copyStreamCudaToGl));
		// TODO should we a cudaEventRecord to say we have finished copying? not needed because cuda is just producing and copying to us

        doUnmap(0);

        //-------------------------------------------------------------------
        CUDA_CHECK(cudaSetDevice(g_cudaDeviceIndex));

        doRender(iter, g_dataGLId[0]);
        
        glFlush();
    }

	// glFinish is necessary call to make when doing CPU based timings to make
	// sure that all the preceding OpenGL commands are done before we finish timing.
	glFinish();

	// cudaStreamSynchronize is necessary call to make when doing CPU based timings to make
	// sure that all the preceding CUDA commands are done before we finish timing.
	CUDA_CHECK(cudaDeviceSynchronize());

    float elapsedMsec = timer.value()*1000;
    nvtxRangePop();

    //-------------------------------------------------------------------
    // cleanup...

    CUDA_CHECK(cudaSetDevice(g_glDeviceIndex));
	unregisterResources();
	CUDA_CHECK(cudaStreamDestroy(copyStreamCudaToGl));
    //CUDA_CHECK(cudaStreamDestroy(copyStreamGlToCuda));
	//CUDA_CHECK(cudaEventDestroy(copyEventGlToCudaComplete));

    CUDA_CHECK(cudaSetDevice(g_cudaDeviceIndex));
	CUDA_CHECK(cudaEventDestroy(kernelEventComplete));
    CUDA_CHECK(cudaStreamDestroy(stream));
    CUDA_CHECK(cudaFree(d_buffer));

	elapsedMsec /= nTimingIterations;
	return elapsedMsec;
}

//---------------------------------------------------------------------------------------------------
