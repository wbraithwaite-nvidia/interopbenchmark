/* ---------------------------------------------------------------------------
 * This software is in the public domain, furnished "as is", without technical
 * support, and with no warranty, express or implied, as to its usefulness for
 * any purpose.

 * Author: Wil Braithwaite.
 *
 */

#ifndef GL_UTILS_H_INCLUDED
#define GL_UTILS_H_INCLUDED


#include <GL/glew.h>

#if defined(_WIN32)
#include <GL/wglew.h>
#else
#include <GL/glx.h>
#include <X11/Xlib.h>
#endif

#define GL_CHECK_ERRORS() Easy::__glCheckErrors(__FILE__,__LINE__)

namespace Easy
{

//---------------------------------------------------------------------------------------
#if defined(_WIN32)

struct GlContextData
{
	HDC hdc;
	HGLRC hglrc;
};

#else

struct GlContextData
{
	Display* xdisplay;
	GLXContext glxcontext;
	Window xwindow;
};

#endif

bool __glCheckErrors(const char *file, const int line);
GlContextData getGlCurrentContextData();
void* getGlCurrentContext();
void* getGlCurrentDisplay();
bool createGlContext(GlContextData* data, bool share=false);
void destroyGlContext(GlContextData* data);
void startGlContext(GlContextData* data);
void endGlContext(GlContextData* data);

void drawGlRectangle(const float lx, const float ly, const float hx, const float hy);
void drawGlText(GLfloat x, GLfloat y, const char *s, void *font=0);

}

#endif // GL_UTILS_H_INCLUDED