/*
 * Copyright 1993-2012 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

#include "interop.h"

using namespace Easy;

//---------------------------------------------------------------------------------------------------
float benchmark_Manual_WriteDiscard_Async_PingPong(int nTimingIterations)
{
    CUDA_CHECK(cudaSetDevice(g_cudaDeviceIndex));

    cudaStream_t stream[2];
    CUDA_CHECK(cudaStreamCreate(&stream[0]));
    CUDA_CHECK(cudaStreamCreate(&stream[1]));

    cudaEvent_t kernelEventComplete[2];
    cudaEvent_t copyEventCudaToHostComplete[2];
    cudaEvent_t copyEventHostToCudaComplete[2];
    CUDA_CHECK(cudaEventCreate(&kernelEventComplete[0]));
    CUDA_CHECK(cudaEventCreate(&kernelEventComplete[1]));
    CUDA_CHECK(cudaEventCreate(&copyEventCudaToHostComplete[0]));
    CUDA_CHECK(cudaEventCreate(&copyEventCudaToHostComplete[1]));
    CUDA_CHECK(cudaEventCreate(&copyEventHostToCudaComplete[0]));
    CUDA_CHECK(cudaEventCreate(&copyEventHostToCudaComplete[1]));

    // create the references on the display GPU...
    CUDA_CHECK(cudaSetDevice(g_glDeviceIndex));
	CUDA_CHECK(cudaFree(0));
    registerResources(false, true);
    cudaStream_t copyStreamGl[2];
    CUDA_CHECK(cudaStreamCreate(&copyStreamGl[0]));
    CUDA_CHECK(cudaStreamCreate(&copyStreamGl[1]));
    //cudaEvent_t kernelEventComplete;
    //cudaEvent_t copyEventCudaToHostComplete;
    //cudaEvent_t copyEventHostToCudaComplete;
    cudaEvent_t copyEventHostToGlComplete[2];
    cudaEvent_t copyEventGlToHostComplete[2];
    //CUDA_CHECK(cudaEventCreate(&kernelEventComplete));
    //CUDA_CHECK(cudaEventCreate(&copyEventCudaToHostComplete));
    //CUDA_CHECK(cudaEventCreate(&copyEventHostToCudaComplete));
    CUDA_CHECK(cudaEventCreate(&copyEventHostToGlComplete[0]));
    CUDA_CHECK(cudaEventCreate(&copyEventHostToGlComplete[1]));
    CUDA_CHECK(cudaEventCreate(&copyEventGlToHostComplete[0]));
    CUDA_CHECK(cudaEventCreate(&copyEventGlToHostComplete[1]));

    CUDA_CHECK(cudaSetDevice(g_cudaDeviceIndex));

    void* d_buffer[2] = {0,0};
    CUDA_CHECK(cudaMalloc(&d_buffer[0], g_copySize));
    CUDA_CHECK(cudaMalloc(&d_buffer[1], g_copySize));
    
    void* h_dataPtr[2] = {0,0};
    CUDA_CHECK(cudaHostAlloc((void**)&h_dataPtr[0], g_copySize, cudaHostAllocPortable));//|cudaHostAllocWriteCombined));
    CUDA_CHECK(cudaHostAlloc((void**)&h_dataPtr[1], g_copySize, cudaHostAllocPortable));//|cudaHostAllocWriteCombined));

    // glFinish is necessary call to make when doing CPU based timings to ensure 
    // that all the preceding OpenGL commands are done before we start timing.
	glFinish();

    // cudaStreamSynchronize is necessary call to make when doing CPU based timings to 
    // ensure that all the preceding CUDA commands are done before we start timing.
	CUDA_CHECK(cudaDeviceSynchronize());
	
    nvtxRangePush("manual-pingpong");
    Timer timer;

    glQueryCounter(g_glTimeStampStart, GL_TIMESTAMP);
    glFlush();

    void* d_mappedPtr;
    int readIndex = 1;
    int writeIndex = 0;

    for (int iter=-1; iter<nTimingIterations; ++iter) 
    {
        if (iter < nTimingIterations-1)
        {
            // wait for previous kernel.
            CUDA_CHECK(cudaStreamWaitEvent(stream[writeIndex], kernelEventComplete[readIndex], 0));

            // run kernel A
            doKernel(iter, d_buffer[writeIndex], stream[writeIndex]);
            CUDA_CHECK(cudaEventRecord(kernelEventComplete[writeIndex], stream[writeIndex]));

            // wait for gl to finish copying the data from the host.
            CUDA_CHECK(cudaStreamWaitEvent(stream[writeIndex], copyEventHostToGlComplete[writeIndex], 0));

            CUDA_CHECK(cudaMemcpyAsync(h_dataPtr[writeIndex], d_buffer[writeIndex], g_copySize, cudaMemcpyDeviceToHost, stream[writeIndex]));
            CUDA_CHECK(cudaEventRecord(copyEventCudaToHostComplete[writeIndex], stream[writeIndex]));
        }

        if (iter >= 0)
        {
            CUDA_CHECK(cudaSetDevice(g_glDeviceIndex));

            doMap((void**)&d_mappedPtr, copyStreamGl[0]);

            // copy the host-memory to the GL-resource's mapped device-memory.
            CUDA_CHECK(cudaStreamWaitEvent(copyStreamGl[0], copyEventCudaToHostComplete[readIndex], 0));

            CUDA_CHECK(cudaMemcpyAsync(d_mappedPtr, h_dataPtr[readIndex], g_copySize, cudaMemcpyHostToDevice, copyStreamGl[0]));
            CUDA_CHECK(cudaEventRecord(copyEventHostToGlComplete[readIndex], copyStreamGl[0]));

            // this will stall until "copyEventCudaToHostComplete".
            doUnmap(copyStreamGl[0]);

            // we need to ensure GL is finished before the Gl-resource is copied to the host.
            CUDA_CHECK(cudaStreamSynchronize(copyStreamGl[0]));

            //----------------------------------------------------------------------------
            CUDA_CHECK(cudaSetDevice(g_cudaDeviceIndex));

            doRender(iter, g_dataGLId[0]);

            glFlush();

        }

        readIndex ^= writeIndex;
        writeIndex ^= readIndex;
        readIndex ^= writeIndex;
    }

	// glFinish is necessary call to make when doing CPU based timings to make
	// sure that all the preceding OpenGL commands are done before we finish timing.
	glFinish();

	// cudaStreamSynchronize is necessary call to make when doing CPU based timings to make
	// sure that all the preceding CUDA commands are done before we finish timing.
	CUDA_CHECK(cudaDeviceSynchronize());

    float elapsedMsec = timer.value()*1000;
    nvtxRangePop();

    CUDA_CHECK(cudaEventDestroy(kernelEventComplete[0]));
    CUDA_CHECK(cudaEventDestroy(kernelEventComplete[1]));
    CUDA_CHECK(cudaEventDestroy(copyEventCudaToHostComplete[0]));
    CUDA_CHECK(cudaEventDestroy(copyEventCudaToHostComplete[1]));
    CUDA_CHECK(cudaEventDestroy(copyEventHostToCudaComplete[0]));
    CUDA_CHECK(cudaEventDestroy(copyEventHostToCudaComplete[1]));
    CUDA_CHECK(cudaStreamDestroy(stream[0]));
    CUDA_CHECK(cudaStreamDestroy(stream[1]));

    CUDA_CHECK(cudaSetDevice(g_glDeviceIndex));
    unregisterResources();
    CUDA_CHECK(cudaStreamDestroy(copyStreamGl[0]));
    CUDA_CHECK(cudaStreamDestroy(copyStreamGl[1]));
    CUDA_CHECK(cudaEventDestroy(copyEventHostToGlComplete[0]));
    CUDA_CHECK(cudaEventDestroy(copyEventHostToGlComplete[1]));
    CUDA_CHECK(cudaEventDestroy(copyEventGlToHostComplete[0]));
    CUDA_CHECK(cudaEventDestroy(copyEventGlToHostComplete[1]));

    CUDA_CHECK(cudaSetDevice(g_cudaDeviceIndex));
    CUDA_CHECK(cudaFreeHost(h_dataPtr[0]));
    CUDA_CHECK(cudaFreeHost(h_dataPtr[1]));
    CUDA_CHECK(cudaFree(d_buffer[0]));
    CUDA_CHECK(cudaFree(d_buffer[1]));

	elapsedMsec /= nTimingIterations;
	return elapsedMsec;
}

//---------------------------------------------------------------------------------------------------
