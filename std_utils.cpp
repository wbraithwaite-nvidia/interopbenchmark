/* ---------------------------------------------------------------------------
 * This software is in the public domain, furnished "as is", without technical
 * support, and with no warranty, express or implied, as to its usefulness for
 * any purpose.

 * Author: Wil Braithwaite.
 *
 */

#include "std_utils.h"
#include <sstream>
#include <sys/types.h>
#include <signal.h>
#include <errno.h>

using namespace std;

namespace Easy
{

//---------------------------------------------------------------------------------------------------
void promptForKey()
{
    printf("\nPress any key to continue...\n\n");
#if defined(_WIN32)
    _getch();
#endif
    return;
}

//-----------------------------------------------------------------------------------
Stringf::Stringf(const std::string& format, ...)
{
    char temp[10000];
    va_list args;
    va_start(args, format);
    vsprintf(temp, format.c_str(), args);
    va_end(args);
    ((std::string *)this)->operator=(temp);
}

//-----------------------------------------------------------------------------------
Stringf::Stringf(const char *format,...)
{
    char temp[10000];
    va_list args;
    va_start(args, format);
    vsprintf(temp, format, args);
    va_end(args);
    ((std::string *)this)->operator=(temp);
}

//-----------------------------------------------------------------------------------
StringArray::StringArray()
{
}

//-----------------------------------------------------------------------------------
StringArray::StringArray(const char* s, const char* seperators)
{
    set(std::string(s), std::string(seperators));
}

//-----------------------------------------------------------------------------------
StringArray::StringArray(const std::string& s, const std::string& seperators)
{
    set(s, seperators);
}

//-----------------------------------------------------------------------------------
bool StringArray::contains(const std::string& s)
{
    for (const_iterator it=begin(); it!=end(); ++it)
        if (*it == s)
            return true;
    return false;
}

//-----------------------------------------------------------------------------------
void StringArray::set(const std::string& s, const std::string& seperators)
{
    clear();
    std::string remaining = s;
    const char *sep = seperators.c_str();

    int n = strspn(remaining.c_str(), sep);
    if (n != 0)
        remaining = remaining.substr(n, remaining.length()-n);

    for (;;)
    {
        int n = strcspn(remaining.c_str(), sep);
        if (n==0)
            break;

        push_back(remaining.substr(0, n));

        remaining = remaining.substr(n, remaining.length()-n);

        n = strspn(remaining.c_str(), sep);
        if (n == 0)
            break;

        remaining = remaining.substr(n, remaining.length()-n);
    }
}

//-----------------------------------------------------------------------------------
bool isFile(const std::string& path)
{
    if (path.empty())
        return false;

#if defined( _WIN32 )
    return (GetFileAttributesA(path.c_str()) != INVALID_FILE_ATTRIBUTES);
#else
    return (access(path.c_str(), F_OK|R_OK) == 0);
#endif
}

//-----------------------------------------------------------------------------------
#if defined(_WIN32)
//unsigned __stdcall thread_func(void *t)
DWORD WINAPI thread_func( LPVOID t )
#else
void *thread_func(void *t)
#endif
{
    Thread *task = (Thread *)t;

#if defined(_WIN32)
    //prctl(PR_TERMCHILD);
#else
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS,NULL);
#endif

    task->_isComplete = false;
    task->onExecute();
    task->_isComplete = true;

    task->_returnCode = 0;

#if defined(_WIN32)
    //_endthreadex(0);
#else
    try
    {
        pthread_exit(&task->_returnCode);
    }
    catch(...)
    {
        throw;
    }
#endif

    return (0);
}

//-----------------------------------------------------------------------------------
Thread::Thread()
{
    _thread = 0;
}

//-----------------------------------------------------------------------------------
Thread::~Thread()
{
    killWait();
#if defined(_WIN32)
    CloseHandle(_thread);
#else
#endif
}

//-----------------------------------------------------------------------------------
bool Thread::execute()
{
#if defined(_WIN32)
    //thread = (HANDLE)beginthreadex(NULL, 0, (unsigned (*)(void *))thread_func, this, 0, NULL);
    _thread = CreateThread(NULL, 0, thread_func, this, 0, NULL);
    if (_thread == 0)
        return false;
#else
    int r = pthread_create(&_thread, NULL, thread_func, this);
    if (r != 0)
    {
        perror("Couldn't create thread!");
        return false;
    }

#endif
    return true;
}

//-----------------------------------------------------------------------------------
void Thread::wait()
{
    if (!_thread)
        return;
#if defined(_WIN32)
    WaitForSingleObject(_thread, INFINITE );
    //CloseHandle(thread);
#else
    int rc=pthread_join(_thread,NULL);
    if (rc!=0)
    {
        printf("pthread error code: %d\n",rc);
    }

    _thread = 0;
#endif
}

//-----------------------------------------------------------------------------------
void Thread::kill()
{
    if (!_thread)
        return;
#if defined(_WIN32)
    TerminateThread(_thread, 0);
#else

    if (_isComplete == false)
    {
        pthread_cancel(_thread);
    }
#endif
}

//-----------------------------------------------------------------------------------
void Thread::killWait()
{
    kill();
    wait();
}

//-----------------------------------------------------------------------------------
bool Thread::isCompleted()
{
#if defined(_WIN32)
    return(_isComplete);
#else
    return(_isComplete);
#endif
}

}
