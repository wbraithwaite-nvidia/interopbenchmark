/*
 * Copyright 1993-2012 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

#ifndef UTILS_H
#define UTILS_H

#include <GL/glew.h>

#if defined(_WIN32)
#include <windows.h>
#include <stdint.h>
#include <conio.h> // for _getch
#define strcasecmp _stricmp

#else /* _UNIX */
#include <sys/time.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <assert.h>
#include <string.h>

#endif /* _UNIX */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <cuda_runtime_api.h>
#include <nvToolsExtCudaRt.h>

#define CUDA_CHECK_ERRORS() \
    do { \
        cudaError err = cudaGetLastError(); \
        if (cudaSuccess != err) { \
            fprintf(stderr, "Cuda error in file '%s' in line %i : %s.\n", __FILE__, __LINE__, cudaGetErrorString(err) ); \
            abort(); \
        } \
    } while(0) 


//---------------------------------------------------------------------------------------
#define CUDA_CHECK(err) _CUDA_CHECK(err, __FILE__, __LINE__)

inline bool _CUDA_CHECK( cudaError err, const char *file, const int line )
{
	if ( cudaSuccess != err)
	{
        fprintf(stderr, "CUDA_CHECK() Runtime API error (%d) in file <%s>, line %i : %s.\n",
			err, file, line, cudaGetErrorString( err) );
        return false;
    }
	return true;
}


#if defined(_WIN32)

inline bool QueryPerformanceFrequency(int64_t *frequency)
{
	LARGE_INTEGER f;
	bool ret = (QueryPerformanceFrequency(&f) == TRUE);
	*frequency = f.QuadPart;
	return ret;
}
inline bool QueryPerformanceCounter(int64_t *performance_count)
{
	LARGE_INTEGER p;
	bool ret = (QueryPerformanceCounter(&p) == TRUE);
	*performance_count = p.QuadPart;
	return ret;
}

#else  /* _UNIX */
/* Helpful conversion constants. */
static const unsigned usec_per_sec = 1000000;


/* These functions are written to match the win32
   signatures and behavior as closely as possible.
*/
inline bool QueryPerformanceFrequency(int64_t *frequency)
{
    /* Sanity check. */
    assert(frequency != NULL);

    /* gettimeofday reports to microsecond accuracy. */
    *frequency = usec_per_sec;

    return true;
}

inline bool QueryPerformanceCounter(int64_t *performance_count)
{
    struct timeval time;

    /* Sanity check. */
    assert(performance_count != NULL);

    /* Grab the current time. */
    gettimeofday(&time, NULL);
    *performance_count = time.tv_usec + /* Microseconds. */
                         time.tv_sec * usec_per_sec; /* Seconds. */

    return true;
}
#endif /* _UNIX */
//
// Return nanosecond clock value.
//
inline int64_t getNanoClock()
{
	int64_t now;
	static int64_t frequency;
	static int gotfrequency = 0;
	int64_t seconds, nsec;

	QueryPerformanceCounter(&now);
	if (gotfrequency == 0) {
		QueryPerformanceFrequency(&frequency);
		gotfrequency = 1;
	}

	seconds = now / frequency;
	nsec = (1000000000 * (now - (seconds * frequency))) / frequency;
    
	return seconds * 1000000000 + nsec;
}

/// Each step: "eval(inputValue, numIterations)" and get the average result over numIterations.
/// As the inputValue is tweaked, the resulting output from eval is either closer or further from targetValue.
/// Each step converges on the target value, and we break when close enough, or when maxSteps is reached.
///
float tuneLinearKnob(float* inputValue, float (*func)(int), int numIterations, float targetValue, int maxSteps=3, int verbosity=0);



#endif
