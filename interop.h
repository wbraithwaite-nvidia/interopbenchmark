/*
 * Copyright 1993-2012 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

#ifndef BENCHMARKINTEROP_H
#define BENCHMARKINTEROP_H

#include "gl_utils.h"
#include "std_utils.h"
#include "utils.h"

#include <GL/freeglut.h>
#include <cuda_runtime_api.h>
#include <cuda_gl_interop.h>
#include <cuda_profiler_api.h>

//---------------------------------------------------------------------------------------------------
/// variables used in the benchmarking tests...
///
extern bool g_useGLsync;
extern bool g_useCudaSync; 
extern unsigned int g_copySize;

extern GLuint g_dataGLId[2];
extern GLsync g_glSyncId;

extern GLuint g_glTimeStampStart;
extern GLuint* g_glTimeStamps;

extern float g_nDelayIterations_CUDA;
extern float g_nDelayIterations_GL;
extern float g_nDelayIterations_Copy;

// The current device ordinals...
extern int g_cudaDeviceIndex;
extern int g_glDeviceIndex;

//---------------------------------------------------------------------------------------------------
/// functions used in the benchmarking tests...
///
void registerResources(bool readable, bool writeable);
void unregisterResources();
void doRender(int frame, GLuint id);
void doKernel(int frame, void* buffer, cudaStream_t stream);
void doMap(void** ptr, cudaStream_t stream);
void doUnmap(cudaStream_t stream);
void doWorkInCUDA(int iter, int numIterations, void* d_dataPtr, size_t dataSize, cudaStream_t stream);
void doWorkInGL(int frame, int nDelayIterations, GLuint id);
void drawLogo(const int frame);


//---------------------------------------------------------------------------------------------------
/// benchmarking tests...
///
typedef float (*BenchmarkFunc)(int);

extern float benchmark_CUDA(int nTimingIterations);
extern float benchmark_Copy(int nTimingIterations);
extern float benchmark_GL(int nTimingIterations);
extern float benchmark_Interop(int nTimingIterations);
extern float benchmark_Interop_WriteDiscard(int nTimingIterations);
extern float benchmark_Manual_WriteDiscard(int nTimingIterations);
extern float benchmark_Manual_WriteDiscard_Async(int nTimingIterations);
extern float benchmark_Manual_WriteDiscard_Async_PingPong(int nTimingIterations);
extern float benchmark_Manual_WriteDiscard_Async_P2P(int nTimingIterations);
//---------------------------------------------------------------------------------------------------

#endif